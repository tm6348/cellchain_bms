#ifdef CENTRAL_MODULE

#define PIN_PWM_OUT PB6
#define PIN_CELLCHAIN_TX PB4
#define PIN_CELLCHAIN_RX PB3


#else

#define PIN_UART_TX PB5
#define PIN_PWM_OUT PB6
#define PIN_CELLCHAIN_TX PB4
#define PIN_CELLCHAIN_RX PB3


#endif