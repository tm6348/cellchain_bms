#ifndef _POWER_MANAGER_H
#define _POWER_MANAGER_H
#include "global.h"
#include "power_manager_locks.h"

typedef void (*power_state_switch_cb_t)(uint32_t);
typedef enum
{
  POWER_MODE_SLEEP = 0,
  POWER_MODE_LOW_PERFORMANCE,
  POWER_MODE_HIGH_PERFORMANCE,
}power_state_t;

typedef struct
{
  volatile power_state_t current_state;
  volatile uint32_t cpu_freq_khz;
  volatile power_state_switch_cb_t switch_cb;
  volatile uint16_t lock_bitfield;
}power_manager_t;

extern power_manager_t power_manager_data;

extern void power_manager_init();

extern void power_manager_set_callback(power_state_switch_cb_t cb);

extern void power_manager_state_lock(uint8_t lock_index);

extern void power_manager_state_unlock(uint8_t lock_index);


extern void power_manager_set_state(power_state_t state);

extern void power_manager_delay_ms(uint16_t ms, power_state_t state);


#endif