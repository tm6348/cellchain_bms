#include "power_manager/power_manager.h"

power_manager_t power_manager_data;
#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__) || defined(__AVR_ATtiny84__)
#define BASE_SPEED 8000
void power_manager_init()
{
  memset(&power_manager_data, 0, sizeof(power_manager_t));
  power_manager_set_state(POWER_MODE_HIGH_PERFORMANCE);
}
void power_manager_state_lock(uint8_t lock_index)
{
  power_manager_data.lock_bitfield |= 1 << lock_index;
}
void power_manager_state_unlock(uint8_t lock_index)
{
  power_manager_data.lock_bitfield &= ~(1 << lock_index);
}
void power_manager_set_callback(power_state_switch_cb_t cb)
{
  power_manager_data.switch_cb = cb;
}
void power_manager_set_state(power_state_t state)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(power_manager_data.lock_bitfield)
    {
      /* Do not change state if locked */
      return;
    }
    if(power_manager_data.current_state == state)
    {
      /* No need to switch since we are already in the required state*/
      return;
    }
    if(POWER_MODE_HIGH_PERFORMANCE == state)
    {    
      /* Full boost! */
      CLKPR = 1 << CLKPCE;
      /* Clock divisor to 0, so we have 8 mhz */
      CLKPR = 1 << CLKPS0;
      power_manager_data.cpu_freq_khz = BASE_SPEED >> 1;
    }
    if(POWER_MODE_LOW_PERFORMANCE == state)
    {
      CLKPR = 1 << CLKPCE;

      CLKPR = 1 << CLKPS0 | 1 << CLKPS1; /* 8 divisor */
      power_manager_data.cpu_freq_khz = BASE_SPEED >> 3;
    }
    if(POWER_MODE_SLEEP == state)
    {
      /*TODO: Implement actual sleep */
      CLKPR = 1 << CLKPCE;

      CLKPR = 1 << CLKPS0 | 1 << CLKPS2; /* 32 divisor */
      power_manager_data.cpu_freq_khz = BASE_SPEED >> 5;
    }
    power_manager_data.current_state = state;

  }
  #ifdef DEBUG_CAT_RECLOCK
  DEBUG_PRINTLN_INT("RECLOCKED to ", power_manager_data.cpu_freq_khz);
  #endif
  if(NULL != power_manager_data.switch_cb)
  {
    power_manager_data.switch_cb(power_manager_data.cpu_freq_khz);
  }
}
#endif

void power_manager_delay_ms(uint16_t ms, power_state_t state)
{
  power_state_t prev_state = power_manager_data.current_state;
  if(!power_manager_data.lock_bitfield)
  {
    power_manager_set_state(state);
  }
  uint32_t required_cycles;
  required_cycles = (uint32_t)(power_manager_data.cpu_freq_khz) * (uint32_t)ms;
  required_cycles = required_cycles >> 2;
  #if 1
  if(required_cycles < UINT16_MAX - 1)
  {
    _delay_loop_2(required_cycles); /* We divide by 4 since one loop takes 4 cycles */
  }
  else
  {
    uint32_t divs = required_cycles / UINT16_MAX;
    uint16_t modulo = required_cycles % UINT16_MAX;
    for(uint32_t i = 0; i < divs; i++)
    {
      _delay_loop_2(UINT16_MAX - 1);
    }
    _delay_loop_2(modulo);
  }
  #endif
  #if defined(DEBUG_CAT_POWER_DELAY)
  DEBUG_PRINTLN_INT("dl ", required_cycles);
  DEBUG_PRINTLN_INT("freq ", power_manager_data.cpu_freq_khz);
  DEBUG_PRINTLN_INT("ms ", ms);
  #endif
  if(!power_manager_data.lock_bitfield)
  {
    power_manager_set_state(prev_state);
  }

}