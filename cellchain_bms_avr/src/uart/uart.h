#ifndef _UART_H
#define _UART_H

#include "global.h"
#include "global_cfg.h"

#define UART_TX_BUFFER_SIZE 128
#define UART_RX_BUFFER_SIZE 128

typedef struct 
{
	uint8_t tx_buffer[UART_TX_BUFFER_SIZE];
  uint8_t rx_buffer[UART_RX_BUFFER_SIZE];
  volatile uint8_t* tx_p;
  volatile uint8_t* rx_p;
  volatile uint8_t tx_len;
  volatile uint8_t tx_pos;
  volatile uint8_t rx_len;
  volatile uint8_t rx_newline_pos;
  volatile bool transmitting;
  volatile bool receiving;
  volatile bool enable_echo;

}uart_data_t;

extern uart_data_t uart_data;


#ifdef ENABLE_UART

void uart_init();

void uart_reclock(uint16_t cpu_freq_khz);

void uart_set_echo(bool echo);

void uart_insert(uint8_t *buf, uint16_t len);

void uart_insert_string(char *buf);

void uart_send();

char* uart_readline();

#endif

#endif