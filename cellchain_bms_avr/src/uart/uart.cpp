#include "uart.h"
#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
/* Baudrate is hardcoded to 9600 */
#define UART_PERIPH_INIT {UCSR0A = 1 << U2X0; /* Double speed */\
UCSR0B = 1 << RXCIE0 | 1 << TXCIE0 | 1 << RXEN0 | 1 << TXEN0;/* Enable rx tx and interrupts */ \
UCSR0C = 1 << UCSZ01 | UCSZ02;} /* 8 bit */
#define UART_DATA_TX_REG UDR0
#define UART_DATA_RX_REG UDR0
#define UART_RX_INT_ISR ISR(USART_RX_vect)
#define UART_TX_INT_ISR ISR(USART_TX_vect)
#define UART_BAUDRATE_REG UBRR0
uart_data_t uart_data;

UART_RX_INT_ISR
{
  char rx_byte = UART_DATA_RX_REG;
  #ifdef DEBUG_CAT_UART_RX
  DEBUG_PRINTLN_INT("URX ", rx_byte);
  #endif
  if(uart_data.enable_echo)
  {
    UART_DATA_TX_REG = rx_byte;
  }
  if(!uart_data.receiving)
  {
    power_manager_state_lock(POWER_MANAGER_LOCK_UART);
    uart_data.rx_len = 0;
    uart_data.rx_newline_pos = 0;
    uart_data.receiving = true;
  }
  uart_data.rx_p[uart_data.rx_len] = rx_byte;
  if('\n' == rx_byte)
  {
    #ifdef DEBUG_CAT_UART_RX
    DEBUG_PRINTLN_INT("URX NL", uart_data.rx_len)
    #endif
    uart_data.rx_newline_pos = uart_data.rx_len;
  }
  uart_data.rx_len++;
  if(uart_data.rx_len >= UART_RX_BUFFER_SIZE)
  {
    uart_data.rx_len = 0;
  }

}
UART_TX_INT_ISR
{

  uart_data.tx_pos++;  
  #ifdef DEBUG_CAT_UART_TX
  DEBUG_PRINT_INT("UTX ", uart_data.tx_p[uart_data.tx_pos])  
  #endif
  if(uart_data.tx_pos >= uart_data.tx_len)
  {
    uart_data.transmitting = false;
    uart_data.tx_len = 0;
    uart_data.tx_pos = 0;

    if(!uart_data.receiving)
    {
      power_manager_state_unlock(POWER_MANAGER_LOCK_UART);
    }
    return;
  }
  UART_DATA_TX_REG = uart_data.tx_p[uart_data.tx_pos];
}

void uart_reclock(uint16_t cpu_freq_khz)
{
  if(8000 == cpu_freq_khz)
  {
    UART_BAUDRATE_REG = 103;
  }
  else
  if(4000 == cpu_freq_khz)
  {
    UART_BAUDRATE_REG = 51;
  }
  else
  if(1000 == cpu_freq_khz)
  {
    UART_BAUDRATE_REG = 12;
  }
}


void uart_init()
{
	uart_data.tx_len = 0;
	uart_data.rx_len = 0;
  uart_data.rx_newline_pos = 0;
  uart_data.tx_pos = 0;
  uart_data.enable_echo = true;
  uart_data.tx_p = uart_data.tx_buffer;

  uart_data.rx_p = uart_data.rx_buffer;

  /* Since we will be enabling interrupts, we should disable interrupts as per manual */
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    UART_PERIPH_INIT
  }


  uart_reclock(power_manager_data.cpu_freq_khz);
}

void uart_insert(uint8_t *buf, uint16_t len)
{

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(len + uart_data.tx_len >= UART_TX_BUFFER_SIZE)
    {
      /* Buffer would overflow */
      return;
    }
    memcpy(uart_data.tx_buffer + uart_data.tx_len, buf, len);
    uart_data.tx_len += len;
  }
}
void uart_set_echo(bool echo)
{
  uart_data.enable_echo = echo;
}
/* TODO: add checks! */
void uart_insert_string(char *buf)
{
  if(NULL == buf)
  {
    return;
  }
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    while(0 != *buf)
    {
      uart_data.tx_p[uart_data.tx_len] = *buf;
      uart_data.tx_len++;
      buf++;
    }
  }
}
void uart_send()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    power_manager_state_lock(POWER_MANAGER_LOCK_UART);
    if(uart_data.transmitting)
    {
      return;
    }
    uart_data.transmitting = true;
    uart_data.tx_pos = 0;
    
    UART_DATA_TX_REG = uart_data.tx_p[uart_data.tx_pos];
  }

}

char* uart_readline()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(0 == uart_data.rx_newline_pos)
    {
      return NULL;
    }
    if(uart_data.rx_newline_pos >= UART_RX_BUFFER_SIZE - 2)
    {
      return NULL;
    }
    /* Finish the string */
    uart_data.rx_p[uart_data.rx_newline_pos + 1] = 0;
    uart_data.rx_newline_pos = 0;
    uart_data.rx_len = 0;
    return (char*)uart_data.rx_buffer;
  }
  return NULL;
}



#endif