#ifndef _GLOBAL_CFG_H

#define NUM_MANAGED_CELLS 1

#ifdef CENTRAL_MODULE
#define ENABLE_CLI
#define ENABLE_UART
//#define ENABLE_DEBUG
#define ENABLE_BENCHMARK_CC 4
#else
//#define ENABLE_DEBUG
#define ENABLE_BENCHMARK_CC 9
#endif

#define FW_VER "v0.1"

#endif