#ifndef _PRINTER_H
#define _PRINTER_H
#include <stdint.h>

void printer_print_msg_num(char *buf_out, uint16_t buf_out_len, int32_t num);

#endif