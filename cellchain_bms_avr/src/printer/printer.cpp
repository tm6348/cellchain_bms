#include "printer.h"
#include <string.h>
#include <stdio.h>
#include <math.h>

void printer_print_msg_num(char *buf_out, uint16_t buf_out_len, int32_t num)
{
  #ifdef USE_SPRINTF
  sprintf(buf_out, "%ld", num);
  #else
  int32_t vm = 1;
  while(vm < num)
  {
    vm *= 10;
  }
  
  
  #endif
}