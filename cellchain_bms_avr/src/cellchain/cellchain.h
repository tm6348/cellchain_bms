#ifndef _CELLCHAIN_H
#define _CELLCHAIN_H

#include "global.h"
#include "cellchain/cellchain_cfg.h"

/* Size of cellchain buffer in bytes */
#define CELLCHAIN_BUFFER_SIZE 20
#define CELLCHAIN_CHECKSUM_LEN 1

/* Defines where the system commands end and application commands start */
#define CELLCHAIN_CMD_START_INDEX 32

typedef enum
{
  CELLCHAIN_ENUMERATION_STATE_NOT_ENUMERATING = 0,
  CELLCHAIN_ENUMERATION_STATE_WAITING_CLEAR,
  CELLCHAIN_ENUMERATION_STATE_DONE_CLEAR,
  CELLCHAIN_ENUMERATION_STATE_WAITING_SET,
}cellchain_enumeration_state_t;
typedef enum
{
  CELLCHAIN_SYS_CMD_ENUMERATE_CLEAR = 0,
  CELLCHAIN_SYS_CMD_ENUMERATE_SET = 1,
  CELLCHAIN_SYS_CMD_BENCHMARK = 2,
  CELLCHAIN_SYS_CMD_STATS = 3,
}cellchain_sys_commands_t;

typedef enum
{
  CELLCHAIN_START = 0, /**< We are waiting for the start bit */
  CELLCHAIN_DATA, /**< We are waiting/receiving data (the receiving progress is tracked in TODO:) */
  CELLCHAIN_STOP, /**< We are stopping the transmission  */
}cellchain_status_t;

typedef enum
{
  CELLCHAIN_ROLE_CONTROL = 0, /**< We are the control device (there can only be one on the bus) */
  CELLCHAIN_ROLE_PERIPHERAL, /**< We are the peripheral device */
}cellchain_role_t;

typedef enum
{
  CELLCHAIN_OFFSET_ADDR = 0,
  CELLCHAIN_OFFSET_COMMAND = 1,
  CELLCHAIN_OFFSET_DATA = 2,
}cellchain_offset_t;

typedef enum
{
  CELLCHAIN_PORT_B = 0,
}cellhain_port_index_t;

typedef struct
{
  uint8_t id;
  uint8_t command;
  uint8_t *data_p;
  uint8_t data_len;
  uint8_t *resp_data_p;
}cellchain_received_msg_t;

typedef void (*cellchain_rx_app_cb_t)(cellchain_received_msg_t*);

typedef struct
{
  cellchain_rx_app_cb_t app_rx_cb;

  cellchain_enumeration_state_t enumeration_state;

  uint8_t id; /**< Id of the cellchain node (relevant in the case if we are a peripheral device) */
  
  int8_t pos_bit; /**< Shared between rx and tx for position of bit within a byte */
  uint8_t pos_byte; /**< Shared between rx and tx for position of byte */
  uint8_t rx_data_len;
  cellchain_status_t status;
  uint16_t period;
  uint16_t period_2;
  uint16_t period_tout;
  bool prev_clock;
  uint8_t *tx_p;
  uint8_t tx_len;
  uint8_t *rx_p;
  uint8_t rx_len;
  uint8_t *tx_data_p;
  uint8_t *rx_data_p;
  uint8_t *cellchain_data_buffer;
  bool transmitting;
  bool need_confirmation; /**< Indicates if we need to confirm the message */
  uint8_t retry_count;
  uint8_t max_retries;
  /* Indicates receiving in progress */
  bool rx_enabled;
  bool rx_in_progress; 
  bool rx_available;
  bool enumerated;
  uint32_t rx_err_count;
  uint16_t int_cnt;
  uint16_t calculated_checksum;

  uint16_t s1_cnt;
  uint16_t s2_cnt;

  uint8_t num_devices;

}cellchain_data_t;




extern volatile cellchain_data_t cellchain_data;
/**
 * @brief Initializes the Cellchain library
 * Initializes the Cellchain protocol on the device. It uses Timer2.
 * @param p_tx_port Pointer to the port of the transmit port
 * @param tx_pin_index Index of the transmit pin in the transmit port
 * @param p_rx_port Pointer to the port of the receive pin
 * @param rx_pin_index Index of the receive pin in the receive port
 */
extern void cellchain_init(cellchain_rx_app_cb_t cb);

extern void cellchain_process();

extern void cellchain_send_message(uint8_t id, uint8_t cmd, uint8_t data_len, bool retry);

extern void cellchain_send_buffer(uint8_t len);

extern void cellchain_rx_enable();

extern void cellchain_rx_disable();

extern void cellchain_wait_for_send();

extern void cellchain_enumerate_bus();


extern uint8_t cellchain_receive();


#endif