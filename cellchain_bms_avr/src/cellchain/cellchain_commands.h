#ifndef _CELLCHAIN_COMMANDS_H
#define _CELLCHAIN_COMMANDS_H
#include "cellchain/cellchain.h"

typedef enum
{
  CELLCHAIN_CMD_CONF_BALANCE = 32, //Set balance parameters
  CELLCHAIN_CMD_SET_BALANCE = 33, //Set balance level (if manually balancing)
  CELLCHAIN_CMD_GET_DATA = 34, //Get readings for battery
  CELLCHAIN_CMD_CALIBRATE = 35, //Get readings for battery

}cellchain_commands_t;

typedef enum
{
  CELLCHAIN_CMD_GET_DATA_VOLT_H =  0,
  CELLCHAIN_CMD_GET_DATA_VOLT_L,
  CELLCHAIN_CMD_GET_DATA_TEMP,
  CELLCHAIN_CMD_GET_DATA_LEN,
}cellchain_cmd_get_data_t;

typedef enum
{
  CELLCHAIN_CMD_CALIBRATE_VOLT_H2 = 0,
  CELLCHAIN_CMD_CALIBRATE_VOLT_H1,
  CELLCHAIN_CMD_CALIBRATE_VOLT_H,
  CELLCHAIN_CMD_CALIBRATE_VOLT_L,
  CELLCHAIN_CMD_CALIBRATE_LEN,
}cellchain_cmd_calibrate_t;

typedef enum
{
  CELLCHAIN_CMD_SET_BALANCE_PCT = 0,
}cellchain_cmd_set_balance_t;

typedef enum
{
  CELLCHAIN_CMD_CONF_BALANCE_LOWER_VOLT_H = 0,
  CELLCHAIN_CMD_CONF_BALANCE_LOWER_VOLT_L,
  CELLCHAIN_CMD_CONF_BALANCE_UPPER_VOLT_H,
  CELLCHAIN_CMD_CONF_BALANCE_UPPER_VOLT_L,
  CELLCHAIN_CMD_CONF_BALANCE_MAX_PCT,
}cellchain_cmd_conf_balance_t;


void cellchain_cmd_entry(cellchain_received_msg_t* msg);

void cellchain_cmd_request_get_data(uint8_t id);
void cellchain_cmd_request_calibrate(uint8_t id, uint32_t mv);

#endif