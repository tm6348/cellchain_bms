/* This file contains the application cellchain commands */

#include "cellchain/cellchain_commands.h"
#include "cell_manager/all_cell_data.h"

#ifdef CENTRAL_MODULE
void cellchain_cmd_resp_get_data(cellchain_received_msg_t* msg)  
{
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN_INT("APP data response from ", msg->id)
  #endif
  all_cell_data.cells[msg->id].voltage_mv = msg->data_p[CELLCHAIN_CMD_GET_DATA_VOLT_H] << 8;
  all_cell_data.cells[msg->id].voltage_mv |= msg->data_p[CELLCHAIN_CMD_GET_DATA_VOLT_L];
  all_cell_data.cells[msg->id].temperature_c = msg->data_p[CELLCHAIN_CMD_GET_DATA_TEMP];
  all_cell_data.cells[msg->id].last_update = system_timer_get_ms();
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN_INT("Volts ", all_cell_data.cells[msg->id].voltage_mv);
  DEBUG_PRINTLN_INT("Temp ", all_cell_data.cells[msg->id].temperature_c);
  #endif
}
void cellchain_cmd_resp_calibrate(cellchain_received_msg_t* msg)
{
  return;
}
void cellchain_cmd_request_get_data(uint8_t id)
{
  cellchain_send_message(id, CELLCHAIN_CMD_GET_DATA, 0, true);
}
void cellchain_cmd_request_calibrate(uint8_t id, uint32_t mv)
{

  cellchain_data.tx_data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H2] = mv >> 24;
  cellchain_data.tx_data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H1] = mv >> 16;
  cellchain_data.tx_data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H] = mv >> 8;
  cellchain_data.tx_data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_L] = mv;
  cellchain_send_message(id, CELLCHAIN_CMD_CALIBRATE, CELLCHAIN_CMD_CALIBRATE_LEN, true);
}
#else
void cellchain_cmd_resp_get_data(cellchain_received_msg_t* msg)
{
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN("APP data request ")
  #endif
  cell_data_t* data = cell_manager_get_cell_data();
  msg->resp_data_p[CELLCHAIN_CMD_GET_DATA_VOLT_H] = data->voltage_mv >> 8;
  msg->resp_data_p[CELLCHAIN_CMD_GET_DATA_VOLT_L] = data->voltage_mv;
  msg->resp_data_p[CELLCHAIN_CMD_GET_DATA_TEMP] = data->temperature_c;


  cellchain_send_message(msg->id, msg->command, CELLCHAIN_CMD_GET_DATA_LEN, false);
  
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN("Sent")
  #endif

}
void cellchain_cmd_resp_calibrate(cellchain_received_msg_t* msg)
{
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN("App calib request")
  #endif
  uint32_t actual_mv;
  actual_mv = (uint32_t)msg->data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H2] << 24;
  actual_mv |= (uint32_t)msg->data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H1] << 16;
  actual_mv |= (uint32_t)msg->data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_H] << 8;
  actual_mv |= (uint32_t)msg->data_p[CELLCHAIN_CMD_CALIBRATE_VOLT_L];
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN_INT("Calibrating with ", actual_mv);
  #endif
  adc_calibrate(actual_mv);
  cellchain_send_message(msg->id, msg->command, 0, false);
}
#endif

void cellchain_cmd_entry(cellchain_received_msg_t* msg)
{
  #ifdef DEBUG_CAT_CELLCHAIN_APP_CMD
  DEBUG_PRINTLN_INT("APP CMD ",msg->command)
  #endif
  if(CELLCHAIN_CMD_GET_DATA == msg->command)
  {
    cellchain_cmd_resp_get_data(msg);
  }
  if(CELLCHAIN_CMD_CALIBRATE == msg->command)
  {
    cellchain_cmd_resp_calibrate(msg);
  }
}