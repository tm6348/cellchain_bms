#include <cellchain/cellchain.h>
volatile cellchain_data_t cellchain_data;

#define NUM_PORTS 1


uint8_t cellchain_tx_buffer[CELLCHAIN_BUFFER_SIZE];

uint8_t cellchain_rx_buffer[CELLCHAIN_BUFFER_SIZE];

/* Chip specifics */

  /* Atmega328P */
  #if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
    /* We use timer 1 TODO: set clock source???*/
    #define CELLCHAIN_TIMER_CLOCK_CFG 0b010
    #define CELLCHAIN_TIMER_INIT(){TCCR1A = 0 << WGM11 | 0 << WGM10; TCCR1B = 1 << WGM12 | 0 << WGM13;\
    TCCR1B |= CELLCHAIN_TIMER_CLOCK_CFG & 0b111;}

    #define CELLCHAIN_TIMER_SET_CNT(value) TCNT1 = value;
    #define CELLCHAIN_TIMER_GET_CNT() TCNT1
  
    /* We use ctc mode in order to have COMPA as ovf */ 
    #define CELLCHAIN_TIMER_OVF_ISR ISR(TIMER1_COMPA_vect)
    #define CELLCHAIN_TIMER_OVF_INT_ENABLE() {TIFR1 |= (1 << OCF1A); TIMSK1 |= 1 << OCIE1A;}
    #define CELLCHAIN_TIMER_OVF_INT_DISABLE() {TIFR1 &= ~(1 << OCF1A); TIMSK1 &= ~(1 << OCIE1A);}

    #define CELLCHAIN_TIMER_COMP0_ISR ISR(TIMER1_COMPB_vect)
    #define CELLCHAIN_TIMER_COMP0_INT_ENABLE() {TIFR1 |= (1 << OCF1B); TIMSK1 |= 1 << OCIE1B;}
    #define CELLCHAIN_TIMER_COMP0_INT_DISABLE() {TIFR1 &= ~(1 << OCF1B); TIMSK1 &= ~(1 << OCIE1B);}
    
    #define CELLCHAIN_TIMER_MAX_CNT UINT16_MAX - 2

    #define CELLCHAIN_TIMER_COMP0_SET(period) OCR1B = period;
    #define CELLCHAIN_TIMER_OVF_SET(period) OCR1A = period;

    #define CELLCHAIN_PINCHANGE_ISR ISR(PINCHANGE_INT_GET_GROUP_ISR(CELLCHAIN_CFG_RX_PIN))

    #define CELLCHAIN_PINCHANGE_INT_INIT(){}
    #define CELLCHAIN_PINCHANGE_INT_ENABLE(){PCICR |= 1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN);\
    PCIFR |= 1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN);\
    PINCHANGE_INT_GET_MSK_REG(CELLCHAIN_CFG_RX_PIN) |= 1 << PINCAHNGE_INT_GET_INDEX(CELLCHAIN_CFG_RX_PIN);  }
    #define CELLCHAIN_PINCHANGE_INT_DISABLE() {PCICR &= ~(1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN)); }
  #endif

  /* Attiny 24/44/84 */

  #if defined(__AVR_ATtiny84__)

    #define CELLCHAIN_TIMER_CLOCK_CFG 0b010
    #define CELLCHAIN_TIMER_INIT(){TCCR1A = 0 << WGM11 | 0 << WGM10; TCCR1B = 1 << WGM12 | 0 << WGM13;\
    TCCR1B |= CELLCHAIN_TIMER_CLOCK_CFG & 0b111;}

    #define CELLCHAIN_TIMER_SET_CNT(value) TCNT1 = value;
    #define CELLCHAIN_TIMER_GET_CNT() TCNT1
  
    /* We use ctc mode in order to have COMPA as ovf */ 
    #define CELLCHAIN_TIMER_OVF_ISR ISR(TIM1_COMPA_vect)
    #define CELLCHAIN_TIMER_OVF_INT_ENABLE() {TIFR1 |= (1 << OCF1A); TIMSK1 |= 1 << OCIE1A;}
    #define CELLCHAIN_TIMER_OVF_INT_DISABLE() {TIFR1 &= ~(1 << OCF1A); TIMSK1 &= ~(1 << OCIE1A);}

    #define CELLCHAIN_TIMER_COMP0_ISR ISR(TIM1_COMPB_vect)
    #define CELLCHAIN_TIMER_COMP0_INT_ENABLE() {TIFR1 |= (1 << OCF1B); TIMSK1 |= 1 << OCIE1B;}
    #define CELLCHAIN_TIMER_COMP0_INT_DISABLE() {TIFR1 &= ~(1 << OCF1B); TIMSK1 &= ~(1 << OCIE1B);}

    #define CELLCHAIN_TIMER_MAX_CNT (UINT16_MAX - 2)

    #define CELLCHAIN_TIMER_COMP0_SET(period) OCR1B = period;
    #define CELLCHAIN_TIMER_OVF_SET(period) OCR1A = period;

    #define CELLCHAIN_PINCHANGE_ISR ISR(PINCHANGE_INT_GET_GROUP_ISR(CELLCHAIN_CFG_RX_PIN))

    #define CELLCHAIN_PINCHANGE_INT_INIT(){}
    #define CELLCHAIN_PINCHANGE_INT_ENABLE(){GIMSK |= 1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN) << 4;\
    GIFR |= 1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN) << 4;\
    PINCHANGE_INT_GET_MSK_REG(CELLCHAIN_CFG_RX_PIN) |= 1 << PINCAHNGE_INT_GET_INDEX(CELLCHAIN_CFG_RX_PIN);  }
    #define CELLCHAIN_PINCHANGE_INT_DISABLE() {GIMSK &= ~(1 << PINCHANGE_INT_GET_GROUP(CELLCHAIN_CFG_RX_PIN) << 4); }
  #endif


void cellchain_rx_disable()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    /* Disable receiving */
    cellchain_data.rx_enabled = false;
  }
}
void cellchain_rx_enable()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(cellchain_data.transmitting || cellchain_data.rx_in_progress)
    {
      #ifdef DEBUG_CAT_CELLCHAIN_RX_EN
      DEBUG_PRINTLN(" Try to enCCrx but f")
      DEBUG_PRINTLN_INT( " 1A ",OCR1A)
      DEBUG_PRINTLN_INT( " 1B ",OCR1B)
      DEBUG_PRINTLN_INT( " TCNT1 ",TCNT1)
      DEBUG_PRINTLN_INT( " rxp ", cellchain_data.rx_in_progress)
      #endif
      return;
    }
    /* Enable the pin change interrupt */

    #ifdef DEBUG_CAT_CELLCHAIN_RX_EN
    DEBUG_PRINTLN(" Enabling CC rx")
    #endif
    CELLCHAIN_PINCHANGE_INT_ENABLE()
    CELLCHAIN_TIMER_OVF_SET(CELLCHAIN_TIMER_MAX_CNT)
    cellchain_data.status = CELLCHAIN_START;
    cellchain_data.prev_clock = false;
    cellchain_data.rx_len = 0;
    cellchain_data.rx_available = false;
    cellchain_data.rx_in_progress = false;
    cellchain_data.rx_p = cellchain_rx_buffer;
    cellchain_data.rx_enabled = true;
  }
}

void cellchain_send_done()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {

    cellchain_data.transmitting = false;

    /* Disable transmit */

    CELLCHAIN_TIMER_SET_CNT(0);
    
    CELLCHAIN_TIMER_COMP0_INT_DISABLE();

    cellchain_rx_enable(); 
  }

}

uint16_t calculate_checksum(uint8_t * buf, uint8_t len)
{
  /* Fletcher's checksum */
  uint8_t s1 = 0;
  uint8_t s2 = 0;
  for(uint8_t i = 0; i < len ; i++)
  {
    s1 = (s1 + buf[i]) % 255;
    s2 = (s2 + s1) % 255;
  }
  return (s2 << 8) | s1;
}

typedef enum
{
  CELLCHAIN_PIN_MODE_INPUT = 0,
  CELLCHAIN_PIN_MODE_OUTPUT,
}cellchain_pin_mode_t;

CELLCHAIN_PINCHANGE_ISR
{
  #ifdef ENABLE_BENCHMARK_CC
  WRITE_PIN(ENABLE_BENCHMARK_CC, true);
  #endif

  cellchain_data.int_cnt++;
  /* We read the pin value */
  uint16_t timer_value = CELLCHAIN_TIMER_GET_CNT();
  CELLCHAIN_TIMER_SET_CNT(0);
  bool state = READ_PIN(CELLCHAIN_CFG_RX_PIN);

  #ifdef CELLCHAIN_INVERT_RX
  state = !state;
  #endif

  /* Forwarding of signal */
  #if CELLCHAIN_CFG_DEVICE_TYPE == CELLCHAIN_DEVICE_LOCAL
  #ifndef CELLCHAIN_ALWAYS_FORWARD
  if(cellchain_data.enumerated)
  {
    WRITE_PIN(CELLCHAIN_CFG_TX_PIN, state);
  }
  #else
  WRITE_PIN(CELLCHAIN_CFG_TX_PIN, state); 
  #endif
  #endif

  /* We go to fastest mode */
  power_manager_set_state(POWER_MODE_HIGH_PERFORMANCE);
  power_manager_state_lock(POWER_MANAGER_LOCK_CELLCHAIN);

  #ifdef DEBUG_CAT_CELLCHAIN_RX_FAST
  DEBUG_PRINTLN_INT(" CC rx io  ", state)
  DEBUG_PRINTLN_INT("  stm ", cellchain_data.status)
  DEBUG_PRINTLN_INT("  pclk ", cellchain_data.prev_clock)
  DEBUG_PRINTLN_INT("  tim ", timer_value)
  #endif

  if(CELLCHAIN_START == cellchain_data.status)
  {
    /* We must determine the bit length */
    if (cellchain_data.prev_clock && !state)
    {
      /* We caught the down transition of start bit */
      #ifdef DEBUG_CAT_CELLCHAIN_RX
      DEBUG_PRINTLN_INT(" FPT  ", timer_value)
      #endif
      cellchain_data.rx_in_progress = true;
      cellchain_data.period = timer_value;
      cellchain_data.period_2 = timer_value*2 - timer_value/2;
      cellchain_data.period_tout = timer_value*3;
      /* TODO: Handle properly */
      if(cellchain_data.period_tout > UINT16_MAX || cellchain_data.period_tout < cellchain_data.period)
      {
        cellchain_data.period_tout = UINT16_MAX - 1;
      }
      cellchain_data.status = CELLCHAIN_DATA;
      cellchain_data.pos_bit = 7;
      cellchain_data.pos_byte = 0;
      cellchain_data.rx_len = 0;
      cellchain_data.rx_p[cellchain_data.pos_byte] = 0;
      cellchain_data.prev_clock = false;

      /* We enable the timeout (OVF) interrupt*/
      CELLCHAIN_TIMER_OVF_SET(cellchain_data.period_tout)
      CELLCHAIN_TIMER_OVF_INT_ENABLE()
    }
  }
  else
  if(CELLCHAIN_DATA == cellchain_data.status)
  {


    if(timer_value > cellchain_data.period_2)
    {
      /* We have 2 periods, so we also had a clock */
      #if 1
      cellchain_data.prev_clock = !cellchain_data.prev_clock;
      #endif
    }



    if(!cellchain_data.prev_clock)
    {
      if(!state)
      {
        cellchain_data.rx_p[cellchain_data.pos_byte] |= 1 << cellchain_data.pos_bit;
      }

      cellchain_data.pos_bit--;
      if(cellchain_data.pos_bit < 0)
      {
        cellchain_data.pos_bit = 7;
        if(cellchain_data.rx_len < CELLCHAIN_BUFFER_SIZE - 2)
        {
          #ifdef DEBUG_CAT_CELLCHAIN_RX
          DEBUG_PRINTLN_INT(" CCGot byte ",cellchain_rx_buffer[cellchain_data.pos_byte])
          #endif

          cellchain_data.rx_len++;
          cellchain_data.pos_byte++;

        }
        cellchain_data.rx_p[cellchain_data.pos_byte] = 0;
      }
    
    }
  }

  cellchain_data.prev_clock = !cellchain_data.prev_clock;


  #ifdef ENABLE_BENCHMARK_CC
  WRITE_PIN(ENABLE_BENCHMARK_CC, false);
  #endif
}

uint8_t verify_rx(uint8_t *buffer, uint8_t len)
{
  /* The length includes the checksum */
  uint16_t rx_checksum = buffer[len - 1] | (buffer[len - 2] << 8); 
  uint16_t calc_checksum = calculate_checksum(buffer, len - 2);
  #ifdef DEBUG_CAT_CELLCHAIN_RX_VERIFY 
  DEBUG_PRINTLN("ver")
  DEBUG_PRINTLN_INT("RX check ", rx_checksum);
  DEBUG_PRINTLN_INT("calc check ", calc_checksum);
  #endif
  if(rx_checksum == calc_checksum)
  {
    /* We return the length without the checksum */
    #ifdef DEBUG_CAT_CELLCHAIN_RX_VERIFY
    DEBUG_PRINTLN("VER OK")
    #endif
    return len-2;
  }
  else
  {
    /* Failed to verify so there is no lenght */
    #ifdef DEBUG_CAT_CELLCHAIN_RX_VERIFY
    DEBUG_PRINTLN("VER FAIL")
    #endif
    return 0;
  }
}
void debug_print_message(uint8_t * buf, uint8_t len)
{
  #if 1
  DEBUG_PRINTLN("dmsg:")
  #endif
  for(uint8_t i = 0; i < len; i++)
  {
    #if 1
    DEBUG_PRINT_INT(" ",(unsigned char)buf[i]);
    #endif
  }
  #if 1
  DEBUG_PRINTLN(" ")
  #endif
}
/* The receive timeout interrupt */
CELLCHAIN_TIMER_OVF_ISR
{

#ifdef ENABLE_BENCHMARK_CC
WRITE_PIN(ENABLE_BENCHMARK_CC, true);
#endif

#ifdef ENABLE_BENCHMARK_CC_OVF
WRITE_PIN(ENABLE_BENCHMARK_CC_OVF, true);
#endif

#ifdef DEBUG_CAT_CELLCHAIN_RX
DEBUG_PRINTLN("CC OVF")      
#endif
/* The inactive state was >3 periods */
if(cellchain_data.rx_in_progress)
{
  /* Stop receiving */
  /* Disable overflow interrupt */
  CELLCHAIN_TIMER_OVF_INT_DISABLE()
  /* Disable pin change interrupt */
  CELLCHAIN_PINCHANGE_INT_DISABLE()


  cellchain_data.rx_in_progress = false;
  cellchain_data.status = CELLCHAIN_START;
  /* Compensate for checksum length */
  #ifndef CELLCHAIN_NO_ENCAPSULATE
  if(cellchain_data.rx_len >= 4 && verify_rx(cellchain_data.rx_p, cellchain_data.rx_len))
  {
    cellchain_rx_disable();
    cellchain_data.rx_available = true;
    #ifdef DEBUG_CAT_CELLCHAIN_RX
    DEBUG_PRINTLN_INT(" CC got msg len", cellchain_data.rx_len)      
    #endif
  }
  else
  {
    cellchain_rx_enable();
  }
  #else
    {
      cellchain_rx_disable();
      cellchain_data.rx_available = true;
      #ifdef DEBUG_CAT_CELLCHAIN_RX
      DEBUG_PRINTLN_INT(" CC got noenc", cellchain_data.rx_len)      
      #endif
    }
    #endif
    /* We stopped receiving so we can unlock the power state */
    power_manager_state_unlock(POWER_MANAGER_LOCK_CELLCHAIN);
  }

  #ifdef ENABLE_BENCHMARK_CC
  WRITE_PIN(ENABLE_BENCHMARK_CC, false);
  #endif
  #ifdef ENABLE_BENCHMARK_CC_OVF
  WRITE_PIN(ENABLE_BENCHMARK_CC_OVF, false);
  #endif
}

/* The transmit interrupt */
CELLCHAIN_TIMER_COMP0_ISR
{
  #ifdef ENABLE_BENCHMARK_CC
  WRITE_PIN(ENABLE_BENCHMARK_CC, true);
  #endif
  if(!cellchain_data.transmitting || cellchain_data.rx_enabled)
  {
    return;
  }
  if(!cellchain_data.prev_clock)
  {
    /* This time we should be clock*/
    TOGGLE_PIN(CELLCHAIN_CFG_TX_PIN);
    CELLCHAIN_TIMER_SET_CNT(0);
  }
  else
  {
    if(CELLCHAIN_START == cellchain_data.status)
    {
      WRITE_PIN(CELLCHAIN_CFG_TX_PIN, false);
      cellchain_data.status = CELLCHAIN_DATA;
      cellchain_data.prev_clock = false;
    }
    else if(CELLCHAIN_DATA == cellchain_data.status)
    {
      WRITE_PIN(CELLCHAIN_CFG_TX_PIN, *(cellchain_data.tx_p) & (1 << cellchain_data.pos_bit));
      cellchain_data.pos_bit--;

      if(cellchain_data.pos_bit < 0)
      {
        #ifdef DEBUG_CAT_CELLCHAIN_TX
        DEBUG_PRINTLN("CC tx adv byte")
        DEBUG_PRINTLN_INT("val ", *cellchain_data.tx_p)
        #endif
        cellchain_data.pos_bit = 7;
        cellchain_data.pos_byte++;
        cellchain_data.tx_p++;
      }
    }
    else if(CELLCHAIN_STOP == cellchain_data.status)
    {
      WRITE_PIN(CELLCHAIN_CFG_TX_PIN, false);
      cellchain_send_done();
      cellchain_data.s2_cnt++;
      return;

    }
    if(cellchain_data.pos_byte >= cellchain_data.tx_len)
    {
      cellchain_data.status = CELLCHAIN_STOP;
      cellchain_data.s1_cnt++;
    }

  }
  cellchain_data.prev_clock = !cellchain_data.prev_clock;
  CELLCHAIN_TIMER_SET_CNT(0)

  #ifdef ENABLE_BENCHMARK_CC
  WRITE_PIN(ENABLE_BENCHMARK_CC, false);
  #endif
}



void cellchain_init(cellchain_rx_app_cb_t cb)
{
  #ifdef ENABLE_BENCHMARK_CC
  SET_PINMODE(ENABLE_BENCHMARK_CC, PIN_MODE_OUTPUT);
  #endif
    
  cellchain_data.app_rx_cb = cb;
  /* Set the data buffer pointer */
  #ifndef CELLCHAIN_NO_ENCAPSULATE
  cellchain_data.tx_data_p = cellchain_tx_buffer + CELLCHAIN_OFFSET_DATA;
  cellchain_data.rx_data_p = cellchain_rx_buffer + CELLCHAIN_OFFSET_DATA;
  #else
  cellchain_data.tx_data_p = cellchain_tx_buffer;
  cellchain_data.rx_data_p = cellchain_rx_buffer;
  #endif
  /* Set the non enumerated id */
  cellchain_data.id = 255;

  cellchain_data.enumerated = false;

  SET_PINMODE(CELLCHAIN_CFG_TX_PIN, PIN_MODE_OUTPUT);

  SET_PINMODE(CELLCHAIN_CFG_RX_PIN, PIN_MODE_INPUT);

  /* Enable pullups on RX pin*/
  WRITE_PIN(CELLCHAIN_CFG_RX_PIN, true);

  /* TODO: */

  CELLCHAIN_TIMER_INIT()


  cellchain_rx_enable();

}

uint8_t cellchain_add_header_and_checksum(uint8_t *buffer, uint8_t id, uint8_t command, uint8_t len)
{
  /* We received the pointer to the whole buffer, not just the data portion */
  buffer[CELLCHAIN_OFFSET_ADDR] = id;
  buffer[CELLCHAIN_OFFSET_COMMAND] = command;

  uint16_t checksum;
  checksum = calculate_checksum(buffer, len);
  buffer[len] = checksum >> 8;
  buffer[len + 1] = checksum;
  /* We return the new len (+2) for the checksum */
  return len + 2;
}

void cellchain_send_message(uint8_t id, uint8_t cmd, uint8_t data_len, bool retry)
{
  /* The message already resides in buffer */
  uint8_t out_len = data_len;
  #ifndef CELLCHAIN_NO_ENCAPSULATE
  /* We also need a header */
  out_len = cellchain_add_header_and_checksum(cellchain_tx_buffer, id, cmd, data_len + CELLCHAIN_OFFSET_DATA);
  /* We get the length of the raw message */
  #else
  /*TODO: */
  #endif

  cellchain_data.retry_count = 0;
  cellchain_data.need_confirmation = retry;
  if(retry)
  {
    cellchain_data.max_retries = 5;
  }
  else
  {
    cellchain_data.max_retries = 0;
  }

  
  #ifdef DEBUG_CAT_CELLCHAIN_TX
  DEBUG_PRINTLN_INT("CC send msg len ", out_len);
  #endif
  cellchain_send_buffer(out_len);
  
}

void cellchain_send_buffer(uint8_t len)
{
  power_manager_set_state(POWER_MODE_HIGH_PERFORMANCE);
  power_manager_state_lock(POWER_MANAGER_LOCK_CELLCHAIN);
  uint16_t msg_period = CELLCHAIN_CFG_TX_PERIOD;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    cellchain_rx_disable();

    /* Enable transmitting */
    CELLCHAIN_TIMER_COMP0_SET(msg_period);
    CELLCHAIN_TIMER_OVF_SET(msg_period + 1);
    CELLCHAIN_TIMER_SET_CNT(0);

    CELLCHAIN_TIMER_OVF_INT_DISABLE();

    CELLCHAIN_PINCHANGE_INT_DISABLE();
    
    CELLCHAIN_TIMER_COMP0_INT_ENABLE();

    cellchain_data.transmitting = true;
    cellchain_data.pos_byte = 0;
    /* We decrement the pos bit */
    cellchain_data.pos_bit = 7; 

    cellchain_data.tx_p = cellchain_tx_buffer;

    cellchain_data.status = CELLCHAIN_START;

    WRITE_PIN(CELLCHAIN_CFG_TX_PIN, false);
    cellchain_data.prev_clock = false;
    cellchain_data.period = msg_period;

    cellchain_data.tx_len = len;
  }
  #ifdef DEBUG_CAT_CELLCHAIN_TX
  DEBUG_PRINTLN_INT("CC tx buf len", cellchain_data.tx_len)
  #endif
  #ifdef DEBUG_CAT_CELLCHAIN_TX_MSG
  DEBUG_PRINTLN(" TX")
  debug_print_message(cellchain_data.tx_p, cellchain_data.tx_len);
  #endif
}

void cellchain_enumerate_bus()
{
  cellchain_data.num_devices = 0;
  cellchain_data.enumeration_state = CELLCHAIN_ENUMERATION_STATE_WAITING_CLEAR;
  cellchain_send_message(0, CELLCHAIN_SYS_CMD_ENUMERATE_CLEAR, 0, true);
  
}

#if CELLCHAIN_CFG_DEVICE_TYPE == CELLCHAIN_DEVICE_CENTRAL
void cellchain_sys_cmd_resp_enumerate_clear(cellchain_received_msg_t* msg)
{
  #ifdef DEBUG_CAT_CELLCHAIN_SYS_CMD
  DEBUG_PRINTLN("CC sys clr")
  #endif
  cellchain_data.enumeration_state = CELLCHAIN_ENUMERATION_STATE_DONE_CLEAR;

}
void cellchain_sys_cmd_resp_enumerate_set(cellchain_received_msg_t* msg)
{
  cellchain_data.num_devices= msg->data_p[0];

  #ifdef DEBUG_CAT_CELLCHAIN_SYS_CMD
  DEBUG_PRINTLN_INT("CC sys num dev", cellchain_data.num_devices)
  #endif

}
#endif

#if CELLCHAIN_CFG_DEVICE_TYPE == CELLCHAIN_DEVICE_LOCAL
void cellchain_sys_cmd_resp_enumerate_clear(cellchain_received_msg_t* msg)
{
  #ifdef DEBUG_CAT_CELLCHAIN_SYS_CMD
  DEBUG_PRINTLN("CC sys clr")
  #endif
  cellchain_data.id = 0;

  msg->resp_data_p[0] = 0;
  cellchain_send_message(0, CELLCHAIN_SYS_CMD_ENUMERATE_CLEAR, 1, false);
}
void cellchain_sys_cmd_resp_enumerate_set(cellchain_received_msg_t* msg)
{
  cellchain_data.id = msg->data_p[0] + 1;

  msg->resp_data_p[0] = cellchain_data.id;
  cellchain_data.enumerated = true;
  #ifdef DEBUG_CAT_CELLCHAIN_SYS_CMD
  DEBUG_PRINTLN("CC sys set")
  DEBUG_PRINT_INT("GOT ID ", cellchain_data.id)
  #endif
  cellchain_send_message(0, CELLCHAIN_SYS_CMD_ENUMERATE_SET, 1, false);

}
#endif

void cellchain_process()
{
  if(CELLCHAIN_ENUMERATION_STATE_DONE_CLEAR == cellchain_data.enumeration_state)
  {
    cellchain_data.enumeration_state = CELLCHAIN_ENUMERATION_STATE_WAITING_SET;
    cellchain_data.tx_p[0] = 0;
    cellchain_send_message(0, CELLCHAIN_SYS_CMD_ENUMERATE_SET, 1, true);
  }
  cellchain_receive();
}

#if 1
uint8_t cellchain_receive()
{
  bool avail = false;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(cellchain_data.rx_available)
    {
      #ifdef DEBUG_CAT_CELLCHAIN_RX
      DEBUG_PRINTLN(" CC rx found available")
      #endif
      cellchain_rx_disable();
      avail = true;
      cellchain_data.rx_available = false;
    }
  }
  if(avail)
  {
    #ifdef DEBUG_CAT_CELLCHAIN_RX_MSG
    debug_print_message(cellchain_rx_buffer, cellchain_data.rx_len);
    #endif
    cellchain_received_msg_t msg;
    msg.data_p = cellchain_data.rx_data_p;
    msg.resp_data_p = cellchain_data.tx_data_p;
    #ifndef CELLCHAIN_NO_ENCAPSULATE


    cellchain_data.rx_data_len -= cellchain_data.rx_len - 4; // To substitue for addr, cmd and checksum
  
    msg.data_len = cellchain_data.rx_data_len;
    msg.id = cellchain_rx_buffer[CELLCHAIN_OFFSET_ADDR];
    msg.command = cellchain_rx_buffer[CELLCHAIN_OFFSET_COMMAND];
    /* Check if response to a system command */
    #if CELLCHAIN_CFG_DEVICE_TYPE == CELLCHAIN_DEVICE_LOCAL
    /* Check if this is not intended for us */
    if(!(0 == msg.id || cellchain_data.id == msg.id))
    {
      /* This is not indended for us */
      cellchain_rx_enable();
      return 0;
    }
    #endif

    if(msg.command < CELLCHAIN_CMD_START_INDEX)
    {
      switch(msg.command)
      {
        case CELLCHAIN_SYS_CMD_ENUMERATE_CLEAR:
          cellchain_sys_cmd_resp_enumerate_clear(&msg);
          break;
        case CELLCHAIN_SYS_CMD_ENUMERATE_SET:
          cellchain_sys_cmd_resp_enumerate_set(&msg);
          break;
        case CELLCHAIN_SYS_CMD_STATS:
        
          break;

        case CELLCHAIN_SYS_CMD_BENCHMARK:

          break;
        
        default:

          break;
      }
      cellchain_rx_enable();
      return 0;
    }
    else
    {
      if(NULL != cellchain_data.app_rx_cb)
      {
        cellchain_data.app_rx_cb(&msg);
      }
      cellchain_rx_enable();
      return cellchain_data.rx_data_len;
    }
    #else
    {
      /* No encapsulation */
      cellchain_data.rx_data_len = cellchain_data.rx_len;
      return cellchain_data.rx_data_len;
      cellchain_rx_enable();
    }
    #endif

  }
  cellchain_rx_enable();
  return false;

}

#endif

void cellchain_wait_for_send()
{
  while(cellchain_data.transmitting)
  {
    _delay_us(100);
  }
}
