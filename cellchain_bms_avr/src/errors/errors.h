#ifndef _ERRORS_H
#define _ERRORS_H
#include "global.h"

extern uint16_t active_errors;
typedef enum
{
  ERROR_ADC,
  ERRROR_BALANCE,
  ERROR_WDT,
}error_index_t;

extern void error_set(error_index_t index);

extern void error_clear_all();

extern bool error_check_any();

#endif