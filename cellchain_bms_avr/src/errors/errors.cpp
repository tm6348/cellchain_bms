#include "errors/errors.h"
uint16_t active_errors = 0;


void error_set(error_index_t index)
{
  active_errors |= 1 << (uint16_t)index;
}

void error_clear_all()
{
  active_errors = 0;
}

bool error_check_any()
{
  return active_errors;
}
