#ifndef _PWM_H
#define _PWM_H
#include "global.h"
#include "global_cfg.h"
#include "pwm_cfg.h"

/* Chip specifics */


extern void pwm_init();

extern void pwm_enable();

extern void pwm_disable();

extern uint32_t system_timer_get_ms();

extern void pwm_reclock(uint32_t new_cpu_freq_khz);


extern void pwm_set_duty_pct(uint8_t pct);

#endif