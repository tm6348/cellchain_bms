#include "pwm.h"

  /* Atmega328P */
  #if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
  /* We use timer 0, configure 1kHz pwm */
  #define PWM_CLOCK_CONFIG 0b011 /* 1/64 system clock */
  #define PWM_SYSTEM_CLOCK_DIVIDER_SHIFT 6 /* Shift for divide by 64 */ 
  #define PWM_TIMER_INIT { \
    TCCR0A = 1 << WGM01 | 1 << WGM00; \
    TCCR0B = 1 << WGM02 | PWM_CLOCK_CONFIG << CS00; \
  } /* Configure fast pwm mode with OCR0A as top, dont enable yet */
  #define PWM_PERIOD OCR0A
  #define PWM_ENABLE_ISR { TIMSK0 |= 1 << OCIE0B | 1 << OCIE0A;}
  #define PWM_DISABLE_ISR { TIMSK0 &= ~(1 << OCIE0B | 1 << OCIE0A);}
  #define PWM_FULL_PERIOD 255
  #define PWM_COMPARE OCR0B
  #define PWM_CNT TCNT0
  #define PWM_OVF_ISR ISR(TIMER0_COMPA_vect)
  #define PWM_COMP0_ISR ISR(TIMER0_COMPB_vect)
  #endif

  #if defined(__AVR_ATtiny84__) 
  /* We use timer 0, configure 1kHz pwm */
  #define PWM_CLOCK_CONFIG 0b011 /* 1/64 system clock */
  #define PWM_SYSTEM_CLOCK_DIVIDER_SHIFT 6 /* Shift for divide by 64 */ 
  #define PWM_TIMER_INIT { \
    TCCR0A = 1 << WGM01 | 1 << WGM00; \
    TCCR0B = 1 << WGM02 | PWM_CLOCK_CONFIG << CS00; \
  } /* Configure fast pwm mode with OCR0A as top, dont enable yet */
  #define PWM_PERIOD OCR0A
  #define PWM_ENABLE_ISR { TIMSK0 |= 1 << OCIE0B | 1 << OCIE0A;}
  #define PWM_DISABLE_ISR { TIMSK0 &= ~(1 << OCIE0B | 1 << OCIE0A);}
  #define PWM_FULL_PERIOD 255
  #define PWM_COMPARE OCR0B
  #define PWM_CNT TCNT0
  #define PWM_OVF_ISR ISR(TIM0_COMPA_vect)
  #define PWM_COMP0_ISR ISR(TIM0_COMPB_vect)
  #endif

uint32_t system_timer_ms;
uint16_t current_duty_percent = 0;

bool pwm_enabled = false;
PWM_OVF_ISR
{
  system_timer_ms++;
  if(pwm_enabled)
  {
    WRITE_PIN(PWM_CFG_PWM_PIN, true);
  }
  
}
PWM_COMP0_ISR
{
  WRITE_PIN(PWM_CFG_PWM_PIN, false);
}

void pwm_reclock(uint32_t new_cpu_freq_khz)
{
  uint32_t timer_freq_khz = new_cpu_freq_khz >> PWM_SYSTEM_CLOCK_DIVIDER_SHIFT;
  #ifdef DEBUG_CAT_PWM
  
  DEBUG_PRINT_INT("new freq", timer_freq_khz)
  #endif
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    PWM_PERIOD = timer_freq_khz;
    if(PWM_PERIOD < 5)
    {
      PWM_PERIOD = 5;
    }
  }
  pwm_set_duty_pct(current_duty_percent);
  #ifdef DEBUG_CAT_PWM
  DEBUG_PRINT_INT("PWM reclock period ", PWM_PERIOD)
  DEBUG_PRINT_INT("compare ", PWM_COMPARE)
  #endif
}

void pwm_init()
{
  PWM_TIMER_INIT
  pwm_reclock(power_manager_data.cpu_freq_khz);
  /* Sane defaults */
  PWM_PERIOD = 255;
  PWM_COMPARE = 128;
  system_timer_ms = 0;
  SET_PINMODE(PWM_CFG_PWM_PIN, PIN_MODE_OUTPUT);
  WRITE_PIN(PWM_CFG_PWM_PIN, false);
  pwm_enabled = false;
  PWM_ENABLE_ISR
}

void pwm_enable()
{
  #ifdef DEBUG_CAT_PWM
  DEBUG_PRINT_INT("PWM enabling ", PWM_PERIOD)
  DEBUG_PRINT_INT("compare ", PWM_COMPARE)
  DEBUG_PRINT_INT("counter ", PWM_CNT)
  #endif
  pwm_enabled = true;
}

void pwm_disable()
{
  pwm_enabled = false;
}

uint32_t system_timer_get_ms()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    return system_timer_ms;
  }
  return 0;
}


void pwm_set_duty_pct(uint8_t pct)
{
  if(pct > 100)
  {
    pct = 100;
  }
  uint16_t new_compare = PWM_PERIOD * pct / 100;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    current_duty_percent = pct;
    PWM_COMPARE = new_compare;
    #ifdef DEBUG_CAT_PWM
    DEBUG_PRINT_INT(" PWM setduty period ", PWM_PERIOD)
    DEBUG_PRINT_INT(" compare ", PWM_COMPARE)
    DEBUG_PRINT_INT(" counter ", PWM_CNT)
    #endif
  }
}

