#include "debug.h"
#include "avr/io.h"
#include "avr/interrupt.h"
#include "util/atomic.h"
#include "zoompin.h"
#include <stdbool.h>
#include <string.h>
#include <util/delay.h>


char int_print_buffer[DEBUG_MAX_INT_TEXT_SIZE];

#ifdef ENABLE_DEBUG
struct
{
  uint8_t buffer[DEBUG_BUF_SIZE];
  volatile uint8_t *p_buffer;
  volatile uint16_t send_pos;
  volatile uint16_t len;
  volatile bool sending;
  
}debug_data;
#endif

#if (defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)) && defined(ENABLE_DEBUG)
ISR(SPI_STC_vect)
{
  #if defined ENABLE_DEBUG
  if(debug_data.send_pos >= debug_data.len || debug_data.send_pos >= DEBUG_BUF_SIZE)
  {
    /* Finished sending */
    debug_data.sending = false;
    debug_data.len = 0;
    debug_data.send_pos = 0;
    return;
  }
  SPDR = debug_data.p_buffer[debug_data.send_pos];
  debug_data.send_pos++;
  #endif
}
void debug_init()
{
  DDRB |= 1 << PB2 | 1 << PB3;
  SPCR = 0;
  SPCR |= 1 << MSTR;
  SPCR |= 1 << SPE;
  debug_data.p_buffer = debug_data.buffer;
  DDRB |= 1 << PB5;
}
void debug_insert(uint8_t *buf, uint16_t len)
{
  #if 0
  /* Wait till we send current */
  while(debug_data.sending)
  {

  }
  #endif

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(len + debug_data.len >= DEBUG_BUF_SIZE)
    {
      /* Buffer would overflow */
      return;
    } 
    memcpy(debug_data.buffer + debug_data.len, buf, len);
    debug_data.len += len;
  }
}
void debug_insert_int(int32_t num)
{
  debug_insert((uint8_t*)"%d", 2);
  #if 1
  debug_insert((uint8_t*) &num, sizeof(int32_t));
  #endif
}
void debug_send()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(debug_data.sending)
    {
      /* We are already sending */
      return;
    }
  }
  debug_data.send_pos = 0;
  /* We insert the first byte */
  SPDR = debug_data.p_buffer[debug_data.send_pos];
  debug_data.send_pos++;
  debug_data.sending = true;
  /* Enable the SPI interrupt */
  SPCR |= 1 << SPIE;
}
void debug_print_newline(char* text)
{
  debug_insert((uint8_t *) text, strlen(text));
  debug_insert((uint8_t *)"\n", 1);
  debug_send();
}
void debug_print_int_newline(char* text, int32_t num)
{
  debug_insert((uint8_t *) text, strlen(text));
  debug_insert_int(num);
  debug_insert((uint8_t *)"\n", 1);
  debug_send();
}
void debug_flush()
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    while(!(debug_data.send_pos >= debug_data.len || debug_data.send_pos >= DEBUG_BUF_SIZE))
    {
      /* Botchy implementation */
      SPDR = debug_data.p_buffer[debug_data.send_pos];
      debug_data.send_pos++;
      _delay_ms(25);
    }
    /* Finished sending */
    debug_data.sending = false;
    debug_data.len = 0;
    debug_data.send_pos = 0;
  }
}
#endif

#if (defined(__AVR_ATtiny84__)) && defined(ENABLE_DEBUG)

/* USI with software strobe option */
void debug_init()
{
  SET_PINMODE(4, PIN_MODE_OUTPUT)
  SET_PINMODE(5, PIN_MODE_OUTPUT)
  debug_data.p_buffer = debug_data.buffer;
  USICR |= 1 << USICS1 | 1 << USIWM0;
}
void debug_insert(uint8_t *buf, uint16_t len)
{
  #if 0
  /* Wait till we send current */
  while(debug_data.sending)
  {

  }
  #endif

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(len + debug_data.len >= DEBUG_BUF_SIZE)
    {
      /* Buffer would overflow */
      return;
    } 
    memcpy(debug_data.buffer + debug_data.len, buf, len);
    debug_data.len += len;
  }
}
void debug_insert_int(int32_t num)
{
  debug_insert((uint8_t*)"%d", 2);
  #if 1
  debug_insert((uint8_t*) &num, sizeof(int32_t));
  #endif
}

void debug_print_newline(char* text)
{
  debug_insert((uint8_t *) text, strlen(text));
  debug_insert((uint8_t *)"\n", 1);
  debug_send();
}
void debug_print_int_newline(char* text, int32_t num)
{
  debug_insert((uint8_t *) text, strlen(text));
  debug_insert_int(num);
  debug_insert((uint8_t *)"\n", 1);
  debug_send();
}
void debug_send()
{
  debug_data.sending = true;
  /* We keep insterting bytes*/
  for(uint8_t i = 0 ; i < debug_data.len; i++)
  {
    WRITE_PIN(4, false)
    WRITE_PIN(5, false)
    _delay_loop_2(10);
    USIDR = debug_data.p_buffer[i];
    _delay_loop_2(10);
    for(uint8_t j = 0; j < 16; j++)
    {
      USICR |= 1 << USITC;
      _delay_loop_2(4);
    }
  }
  debug_data.len = 0;
  debug_data.sending = false;
}
void debug_flush()
{
  /* Not required, since the send is blocking anyway */
}
#endif
#ifndef ENABLE_DEBUG

/* USI with software strobe option */
void debug_init()
{

}
void debug_insert(uint8_t *buf, uint16_t len)
{

}
void debug_insert_int(int32_t num)
{

}
void debug_print_newline(char* text)
{
  
}
void debug_print_int_newline(char* text, int32_t num)
{
  
}


void debug_send()
{

}
void debug_flush()
{

}
#endif