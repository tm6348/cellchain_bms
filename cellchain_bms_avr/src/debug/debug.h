#ifndef _DEBUG_H
#define _DEBUG_H
#include "printer/printer.h"
#include "debug_cfg.h"
#include "global.h"
#include <string.h>


#ifdef ENABLE_DEBUG
extern char int_print_buffer[];
#define DEBUG_PRINT(text){debug_insert((uint8_t*) text, strlen(text)); debug_send();}

#if 0
#define DEBUG_PRINTLN(text){debug_insert((uint8_t*) text, strlen(text));\
	debug_insert((uint8_t*) "\n", 1); debug_send();}
#else
#define DEBUG_PRINTLN(text){ debug_print_newline((char *)text); }
#endif
#define DEBUG_PRINT_INT(text, num){debug_insert((uint8_t*) text, strlen(text));\
	debug_insert_int(num);\
	debug_send();}


#if 0
#define DEBUG_PRINTLN_INT(text, num){debug_insert((uint8_t*) text, strlen(text));\
	debug_insert_int(num);\
	debug_insert((uint8_t*) "\n", 1); debug_send();}
#else
#define DEBUG_PRINTLN_INT(text, num){debug_print_int_newline((char *)text, num);}
#endif
#else

#define DEBUG_PRINT(text){}

#define DEBUG_PRINTLN(text){}

#define DEBUG_PRINT_INT(text, num){}
#define DEBUG_PRINTLN_INT(text, num){}
#endif

#if 0
#define DEBUG_CAT_POWER_DELAY
#endif
#if 1
#define DEBUG_CAT_ADC_SUPPLY
#endif

#if 1
#define DEBUG_CAT_ADC_CALIBRATE
#endif

#if 0
#define DEBUG_CAT_PWM
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_TX
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_TX_MSG
#endif

#if 0
#define DEBUG_CAT_CELLCHAIN_RX_MSG
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_RX_VERIFY
#endif

#if 1
#define DEBUG_CAT_CELLCHAIN_SYS_CMD
#endif

#if 1
#define DEBUG_CAT_CELLCHAIN_APP_CMD
#endif

#if 0
#define DEBUG_CAT_CELLCHAIN_RX
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_RX_EN
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_TX_FAST
#endif
#if 0
#define DEBUG_CAT_CELLCHAIN_RX_FAST
#endif

#if 0
#define DEBUG_CAT_RECLOCK
#endif

#if 0
#define DEBUG_CAT_UART_TX
#endif

#if 0
#define DEBUG_CAT_UART_RX
#endif

#if 0
#define DEBUG_CAT_CLI_PARSE
#endif

#if 1
#define DEBUG_CAT_ALL_DATA
#endif

#if 1
#define DEBUG_CAT_BMS_STATE
#endif



void debug_init();


void debug_insert(uint8_t *buf, uint16_t len);

void debug_print_newline(char* text);

void debug_print_int_newline(char* text, int32_t num);

void debug_insert_int(int32_t text);

void debug_send();

void debug_flush();

#endif