#include "adc/adc.h"
#include <avr/eeprom.h>
#define ADC_CLOCK_CONFIG 0b101         /* System clock / 32 */


#define ADC_NUM_TICKS 1024


#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
  #define ADC_SELECT_VCC_REF { ADMUX = ADMUX & 0b00111111; ADMUX |= 0b01000000;}
#endif
#if defined(__AVR_ATtiny84__)
  #define ADC_SELECT_VCC_REF { ADMUX = ADMUX & 0b00111111;}
#endif

adc_data_t adc_data;



adc_result_t batt_result;
adc_result_t temp_result;

uint16_t source_val;
uint16_t ref_val;

void adc_init()
{
  adc_data.ref_channel = ADC_CFG_REF_CHANNEL;
  adc_data.temp_channel = ADC_CFG_TEMP_CHANNEL;
  adc_data.ref_mv_10x = ADC_CFG_REF_mV_10x;
  uint32_t eeprom_data = eeprom_read_dword((uint32_t *)0);
  if(0 != eeprom_data && UINT32_MAX != eeprom_data)
  {
    adc_data.ref_mv_10x = eeprom_data;
  }
}

void adc_start()
{
  ADCSRA |= (1 << ADEN) | (1 << ADSC) | (ADC_CLOCK_CONFIG & 0b111);
  /* Set to VCC reference */
  ADC_SELECT_VCC_REF
  while(ADCSRA & (1 << ADSC))
  {
    /* Wait for initial conversion to finish */
  }

}

void adc_stop()
{
  ADCSRA &= ~(1 << ADEN);
}

float adc_avg_read(uint16_t n)
{
  uint32_t avg = 0;
  uint16_t waits = 0;
  uint16_t current;
  for(uint16_t i = 0; i < n; i++)
  {
    ADCSRA |= 1 << ADSC;


    while(ADCSRA & (1 << ADSC))
    {
      /* Wait for the end of conversion */
      waits++;
    }
    current = ADCL;
    current |= (ADCH << 8);
    avg += current;

  }
  #ifdef DEBUG_CAT_ADC_SUPPLY
  DEBUG_PRINTLN_INT("Adc avgs", avg);
  DEBUG_PRINTLN_INT("Adc last", current);
  #endif
  return (float)avg / n;
}

uint16_t adc_read_supply_mV()
{


  /* Set the input channel to ref*/
  #if 0
  ADMUX &= 0b11110000;
  #endif
  ADC_SELECT_VCC_REF
  ADMUX |= 0b00001111 & adc_data.ref_channel;
  return ((uint32_t)ADC_NUM_TICKS * ((float)adc_data.ref_mv_10x/ 10.0f)) / adc_avg_read(ADC_CFG_AVERAGE_NUM);

}

bool adc_calibrate(uint16_t actual_mv)
{
  if(0 == actual_mv)
  {
    adc_data.ref_mv_10x = ADC_CFG_REF_mV_10x;
    eeprom_write_dword((uint32_t*)0, 0);
    return true;
  }
  ADC_SELECT_VCC_REF
  ADMUX |= 0b00001111 & adc_data.ref_channel;
  float avg = adc_avg_read(ADC_CFG_AVERAGE_NUM);

  #ifdef DEBUG_CAT_ADC_CALIBRATE
  DEBUG_PRINTLN_INT("ADC CAL Avg", avg);
  #endif

  uint32_t required_ref_mv_10x = ((actual_mv * avg) / ADC_NUM_TICKS) * 10; 

  #ifdef DEBUG_CAT_ADC_CALIBRATE
  DEBUG_PRINTLN_INT("ADC CAL Ref", required_ref_mv_10x);
  #endif
  adc_data.ref_mv_10x = required_ref_mv_10x;
  eeprom_write_dword((uint32_t*)0, adc_data.ref_mv_10x);
  return true;
}

int16_t adc_read_temp_C()
{
  #ifdef ADC_CFG_DISABLE_TEMP
  return INT8_MIN;
  #endif
  uint16_t temp_val;
  uint16_t temp_mv;

    /* Set the input channel to temperature*/
  ADMUX &= 0b11110000;
  ADMUX |= 0b00001111 & adc_data.temp_channel;

  temp_val = adc_avg_read(ADC_CFG_AVERAGE_NUM);

  temp_mv = (temp_val * adc_read_supply_mV()) / ADC_NUM_TICKS;

  return temp_mv;

}