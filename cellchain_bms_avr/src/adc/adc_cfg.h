#ifndef _ADC_CFG_H
#define _ADC_CFG_H
#define ADC_CFG_REF_CHANNEL 1
#define ADC_CFG_TEMP_CHANNEL 2
#define ADC_CFG_AVERAGE_NUM 1500
#define ADC_CFG_REF_mV_10x 12400

#if 1
#define ADC_CFG_DISABLE_TEMP
#endif
#endif