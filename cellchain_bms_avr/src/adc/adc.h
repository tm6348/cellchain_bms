#ifndef _ADC_H
#define _ADC_H
#include "global.h"
#include "adc_cfg.h"
typedef struct
{
  uint16_t sum;
  uint8_t count;
  uint16_t mV;
}adc_result_t;


typedef struct
{
  uint8_t ref_channel;
  uint8_t temp_channel;
  int32_t ref_mv_10x;
}adc_data_t;

extern adc_data_t adc_data;

extern void adc_init();


extern void adc_start();
extern void adc_stop();


bool adc_calibrate(uint16_t actual_mv);

uint16_t adc_read_supply_mV();

int16_t adc_read_temp_C();

#endif