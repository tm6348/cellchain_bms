#include "global.h"
#include "main.h"
#include "cellchain/cellchain_commands.h"
#include "cell_manager/all_cell_data.h"

#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__) || defined(__AVR_ATtiny84__) || defined(__AVR_ATtiny85__)
  #define WDT_CLEAR MCUSR &= ~(1 << WDRF); /* Clear wdt reset flag */
#endif


void reclock_handler(uint32_t new_clock_khz)
{
  pwm_reclock(new_clock_khz);
  #ifdef ENABLE_UART
  uart_reclock(new_clock_khz);
  #endif
}
void cellchain_cb(cellchain_received_msg_t* msg)
{
  DEBUG_PRINT_INT("CC CB", msg->command)
}
#ifdef MAIN_TEST_DEBUG
int main()
{
  WDT_CLEAR
  wdt_disable();
  _delay_ms(400);
  sei();
  #if 1
  debug_init();
  DEBUG_PRINTLN("CM start")
  #endif
  #if 1
  adc_init();
  adc_start();
  #endif
  #if 1
  wdt_enable(WDTO_8S);
  #endif
  #if 1
  pwm_init();
  pwm_set_duty_pct(0);
  pwm_enable();
  #endif

  #if 1
  cellchain_init(&cellchain_cmd_entry);
  #endif

  #if 1
  power_manager_init();
  power_manager_set_callback(&reclock_handler);
  power_manager_set_state(POWER_MODE_HIGH_PERFORMANCE);
  power_manager_state_lock(POWER_MANAGER_LOCK_MAIN);
  #endif
  #if 0
  while(1)
  {
    DEBUG_PRINT("a")
    power_manager_delay_ms(100, POWER_MODE_HIGH_PERFORMANCE);
    wdt_reset();
  }
  #endif


  /* Test for flush */
  #if 0
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    while(1)
    {
      DEBUG_PRINTLN_INT("test ",1)
      debug_flush();
      power_manager_delay_ms(500, POWER_MODE_HIGH_PERFORMANCE);
    }
  }
  #endif

  #ifdef ENABLE_UART
  uart_init();
  #endif
  #if 0
  while(1)
  {
    char *newline = NULL;
    newline = (char *)uart_readline();
    if(newline != NULL)
    {
      uart_insert_string(newline);
      cli_wordify(newline);
      char *word = cli_get_word(newline, 2);
      uart_insert_string((char*)"Word0: ");
      uart_insert_string(word);
      uart_insert_string((char*)"\n");
      uart_send();
    }

    wdt_reset();
    power_manager_delay_ms(500, POWER_MODE_LOW_PERFORMANCE);
  }
  #endif
  #ifdef CENTRAL_MOUDLE
  bms_state = BMS_STATE_ENUMERATING;
  #endif
  while(1)
  {

    #ifdef CENTRAL_MODULE
    static uint32_t prev_cnt = 0;
    if(BMS_STATE_IDLE == bms_state)
    {
      if(system_timer_get_ms() - prev_cnt > 500)
      {
        all_cell_data_process();
        prev_cnt = system_timer_get_ms();
      }
    }
    else if(BMS_STATE_ENUMERATING == bms_state)
    {
      if(cellchain_data.num_devices > 0)
      {
        /* We enumerated and go to idle */
        bms_state = BMS_STATE_IDLE;
      }
    }
    #endif
    cellchain_process();
    wdt_reset();
    power_manager_delay_ms(10, POWER_MODE_LOW_PERFORMANCE);
    #if 0
    DEBUG_PRINTLN_INT("sys ms ", system_timer_get_ms())
    #endif
    #ifdef ENABLE_CLI
    cli_process();
    #endif
  }
}
#endif

#if 0
void initialize()
{
  MCUSR &= ~(1 << WDRF);
  wdt_disable();

  pwm_uart_init(PB6);
  cellchain_init();
  #if 0
  PRINTF("Started\n") 
  #endif
  
  wdt_enable(WDTO_500MS);

}


#ifdef CENTRAL_MODULE
int main()
{
  char *line;
  initialize();
  while(1)
  {
    wdt_reset();
    line = uart_readline();
    if(NULL != line)
    {
      //cli_process_command(line);
    }
    uint8_t msg_len = 0;
    if(cellchain_receive(rx_buf, &msg_len))
    {
      cellchain_rx_enable();
    }
    #if 0
  #if 0  
  uart_send("TEST");
    #endif
    _delay_ms(500);
    uint8_t snd = uart_data.sending;
    if(snd)
    {
      #if 0
      uart_send("SND");
      #endif
    }
    else
    {
      #if 0
      uart_send("not");
      #endif
    }
    #endif
  }
}
#else
//TODO
#endif
#endif