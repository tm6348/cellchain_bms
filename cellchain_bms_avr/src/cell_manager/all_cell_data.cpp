#include "cell_manager/all_cell_data.h"
#include "cellchain/cellchain_commands.h"
#ifdef CENTRAL_MODULE

bms_state_t bms_state;
all_cell_data_t all_cell_data;
uint8_t current_cell = 0;
extern bool all_cell_data_process()
{
  all_cell_data.num_cells = cellchain_data.num_devices + 1;
  if(current_cell >= all_cell_data.num_cells)
  {
    current_cell = 0;
    return true;
  }
  #ifdef DEBUG_CAT_ALL_DATA
  DEBUG_PRINTLN_INT("Polling cell ", current_cell)
  #endif
  if(0 == current_cell)
  {
    cell_manager_process();
    all_cell_data.cells[0] = cell_manager_data.managed_cells[0].data;
    
    
  }
  else
  {
    cellchain_cmd_request_get_data(current_cell);
  }
  current_cell++;
  return false;


}
#endif