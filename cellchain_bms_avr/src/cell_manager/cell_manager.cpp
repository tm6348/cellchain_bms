#include "cell_manager/cell_manager.h"

cell_manager_t cell_manager_data;

void cell_manager_init()
{

}
cell_data_t* cell_manager_get_cell_data()
{
  cell_manager_process();
  return &(cell_manager_data.managed_cells[0].data);
}
void cell_manager_process()
{
  for(uint8_t i = 0; i < NUM_MANAGED_CELLS; i++)
  {
    managed_cell_t *cell;
    cell = &(cell_manager_data.managed_cells[0]);
    if(0 == i)
    {
      /* TODO: make adc module modular since we only support one manged cell for now */
      cell->data.voltage_mv = adc_read_supply_mV();
      cell->data.temperature_c = adc_read_temp_C();
      cell->data.last_update = system_timer_get_ms();
      if(cell->data.voltage_mv > cell->lower_balance_mv)
      {
        /* We must balance */
        if(cell->data.voltage_mv > cell->lower_balance_mv)
        {
          /* We are above upper limit so we apply full pwm */
          cell->balance_pwm_pct = 100;
        }
        else
        {
           /* We are not above upper limit so we calculate the pwm */
          cell->balance_pwm_pct = (255 * (cell->data.voltage_mv - cell->lower_balance_mv)) / 
          (cell->upper_balance_mv - cell->lower_balance_mv);
        }
      }
      /* TODO: make pwm more modular */
      pwm_set_duty_pct(cell->balance_pwm_pct);
    }
  }
}