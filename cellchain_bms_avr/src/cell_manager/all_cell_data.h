#ifndef _CELL_INFO_H
#define _CELL_INFO_H
#include "cell_manager/cell_manager.h"
#ifdef CENTRAL_MODULE
#define NUM_CELLS_MAX 128

typedef enum
{
  BMS_STATE_ENUMERATING = 0,
  BMS_STATE_IDLE,
  BMS_STATE_ACTIVE,
}bms_state_t;

typedef struct
{
  uint8_t num_cells;
  cell_data_t cells[NUM_CELLS_MAX];
}all_cell_data_t;

extern bms_state_t bms_state;

/* We always manage the first n cells in the array */
extern all_cell_data_t all_cell_data;


extern bool all_cell_data_process();
#endif

#endif