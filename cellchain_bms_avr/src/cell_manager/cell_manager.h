#ifndef _CELL_MANAGER_H
#define _CELL_MANAGER_H

#include "global.h"
#include "global_cfg.h"


typedef struct
{
  uint16_t voltage_mv;
  int8_t temperature_c;
  uint32_t last_update;
}cell_data_t;
typedef struct
{
  cell_data_t data;
  uint8_t balance_pwm_pct;
  uint16_t upper_balance_mv;
  uint16_t lower_balance_mv;
}managed_cell_t;


typedef struct
{
  managed_cell_t managed_cells[NUM_MANAGED_CELLS];
}cell_manager_t;

extern cell_manager_t cell_manager_data;

extern void cell_manager_init();

extern cell_data_t* cell_manager_get_cell_data();
extern void cell_manager_process();

#endif