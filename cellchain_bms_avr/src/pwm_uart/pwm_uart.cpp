#include <pwm_uart/pwm_uart.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <string.h>

#if 0
volatile pwm_data_t pwm_data;
volatile uart_data_t uart_data;

#define PERIOD_CYCLES 104
#define UART_RX_PIN PB0
#define UART_TX_PIN PB1

#ifdef ENABLE_PRINT
char print_buffer[PRINT_BUFFER_SIZE];
char uart_rx_buffer[RX_BUFFER_SIZE];

#endif

/* Chip specifics */

  /* Atmega328P */
  #if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
    #define HW_UART
    /* We use timer 0 */
    /* Configure CTC (Clear on compare match) mode */
    #define TIMER_CLOCK_CONFIG (1 << CS01 | 1 << CS00)
    #define PWM_UART_TIMER_INIT(){TCCR0A |= (1 << WGM01); TCCR0B |= (TCCR0B & ~0b111) | TIMER_CLOCK_CONFIG; \
    TIMSK0 |= 1 << OCIE0A | 1 << OCIE0B;}
    #define PWM_UART_TIMER_SET_CNT(value){TNCT0 = value;}
    #define PWM_UART_TIMER_GET_CNT() (TCNT0)

    #define PWM_UART_TIMER_COMP0_SET(period) OCR0B = period;
    #define PWM_UART_TIMER_OVF_SET(period) OCR0A = period;

    #define PWM_UART_TIMER_OVF_ISR ISR(TIMER0_COMPA_vect)
    #define PWM_UART_TIME_OVF_INT_ENABLE(){TIFR0 |= (1 << OCF0A); TIMSK0 |= 1 << OCIE0A;  }
    #define PWM_UART_TIMER_OVF_INT_DISABLE() { /* TODO */}

    #define PWM_UART_TIMER_COMP0_ISR ISR(TIMER0_COMPB_vect)
    #define PWM_UART_TIMER_COMP0_INT_ENABLE(){TIFR0 |= (1 << OCF0B); TIMSK0 |= 1 << OCIE0B;  }
    #define PWM_UART_TIMER_COMP0_INT_DISABLE() {/* TODO: */}
    
    #define PWM_UART_TX_ISR ISR(USART_TX_vect)
    #define PWM_UART_RX_ISR ISR(USART_RX_vect)
    #define PWM_UART_DATA_OUT UDR0
  #endif

volatile uint32_t system_tick_counter;

void uart_process();

#ifdef HW_UART
PWM_UART_TX_ISR
{
  if(!uart_data.sending)
  {
    return;
  }
  /* We go to next byte */
  PWM_UART_DATA_OUT = uart_data.buf_p[uart_data.tx_buf_pos_byte];
  uart_data.tx_buf_pos_byte++;
  if(uart_data.tx_buf_pos_byte >= uart_data.tx_buf_len)
  {
    uart_data.sending = false;
    uart_data.tx_buf_len = 0;
  }
}
PWM_UART_RX_ISR
{
  char cur;
  cur = PWM_UART_DATA_OUT;
  uart_rx_buffer[uart_data.rx_buf_pos_byte] = cur;
  uart_data.rx_buf_pos_byte++;
}
#endif

PWM_UART_TIMER_COMP0_ISR
{
  if(pwm_data.enabled)
  {
    WRITE_PIN(PWM_UART_PWM_PIN, 0)
    #if 0
    PORTB &= ~(1 << pwm_data.pin);
    #endif
  }

}
PWM_UART_TIMER_OVF_ISR
{
  if(pwm_data.enabled)
  {
    WRITE_PIN(PWM_UART_PWM_PIN, 1)
    #if 0
    PORTB |= 1 << pwm_data.pin;
    #endif
  }
  system_tick_counter++;

  /* TODO: sw uart */
  /* Reset the timer */
  TCNT0 = 0;
}


extern void advance_system_tick_counter(uint32_t ticks)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    system_tick_counter += ticks;
  }
}
uint32_t get_system_tick_counter()
{
  uint32_t counter;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    counter = system_tick_counter;
  }
  return counter;
  
}

void print_num_i32(const char *text, int32_t num)
{
  uint8_t text_len = strlen(text);    
  uint8_t count = 0;
  char out[16];

  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(uart_data.tx_buf_pos_byte + text_len < PRINT_BUFFER_SIZE)
    {
      memcpy(print_buffer + uart_data. tx_buf_pos_byte, text, sizeof(char) * text_len);
    }
    int32_t vm = num;
    while(vm > 0 && count < 15)
    {
      out[count] = '0' + (vm % 10);
      count++;
      vm = vm / 10;
    }
  }
  while(count > 0)
  {
    print_buffer[uart_data.tx_buf_pos_byte] = out[count];
    uart_data.tx_buf_pos_byte++;
    count--;
  }
  uart_send(print_buffer);
}

void uart_flush()
{
  uint16_t counter = 0;
  while(1)
  {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
      counter++;
      if(!uart_data.sending || counter > 1000)
      {
        return;
      }
    }
    _delay_us(250);
  }

}

void pwm_uart_init(uint8_t pwm_pin)
{
  system_tick_counter = 0;
  #ifdef ENABLE_PRINT
  memset(print_buffer,0 , PRINT_BUFFER_SIZE * sizeof(char));
  #endif
  pwm_data.pin = pwm_pin;
  
  SET_PINMODE(PWM_UART_PWM_PIN, PIN_MODE_OUTPUT);
  #if 0
  DDRB |= (1 << pwm_data.pin);
  #endif

  /*UART init at 9600*/ //TODO maybe
  UBRR0H = 0;
  UBRR0L = 51;
  /* 8 bits, no parity */
  UCSR0C = (0 << UCSZ02) | (1 << UCSZ01) | (1 << UCSZ00);
  
  _delay_ms(5);
  


  /* Configure the timer */
  TIMSK0 |= (1 << OCIE0A) | (1 << OCIE0B);

  TCCR0A |= (1 << WGM01);

  TCCR0B |= (TCCR0B & 0b11111000) | TIMER_CLOCK_CONFIG;

  //TCCR0 = (TCCR0 & 0b11110000) | TIMER_CLOCK_CONFIG;  


  OCR0B = PERIOD_CYCLES / 2;
  OCR0A = PERIOD_CYCLES;

  /* Enable uart rx*/

  UCSR0B |= (1 << RXEN0) | (1 << RXCIE0);

  uart_data.rx_buf_pos_byte = 0;

  sei();
}

void pwm_start()
{
  pwm_data.enabled = true;
}

void pwm_stop()
{
  pwm_data.enabled = false;
}

void pwm_set_duty_pct(uint8_t pct)
{
  if(pct > 100)
  {
    pct = 100;
  }
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    pwm_data.pwm_cycles = PERIOD_CYCLES * pct / 100;
    OCR0B = pwm_data.pwm_cycles;
  }
}


bool uart_send(char * buf_p)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if(NULL == buf_p)
    {
      return false;
    }
    if(uart_data.sending)
    {
      uart_data.tx_buf_len = strnlen((const char*)uart_data.buf_p, PRINT_BUFFER_SIZE);

      return true;
    }
    else
    {
      uart_data.buf_p = buf_p;
      uart_data.tx_buf_len = strnlen((const char*)uart_data.buf_p, PRINT_BUFFER_SIZE);
      if(uart_data.tx_buf_len > PRINT_BUFFER_SIZE)
      {
        return false;
      }
      uart_data.tx_buf_pos_byte = 0;

      /* Enable hardware transmit */
      UCSR0B |= (1 << TXEN0) | (1 << TXCIE0);

      /* Load first byte */
      UDR0 = uart_data.buf_p[uart_data.tx_buf_pos_byte];
      uart_data.tx_buf_pos_byte++;

      uart_data.sending = true;
      return true;
    }
  }
  return false;
}

void uart_clear_rx()
{
  uart_data.rx_buf_pos_byte = 0;
}


char* uart_readline()
{
  #ifdef ENABLE_PRINT
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if('\n' == uart_rx_buffer[uart_data.rx_buf_pos_byte - 1])
    {
      /* Finish the string */
      if(strnlen(uart_rx_buffer, RX_BUFFER_SIZE) > 3)
      {
        uart_data.rx_buf_pos_byte--;
        if('\r' == uart_rx_buffer[uart_data.rx_buf_pos_byte - 1])
        {
          uart_data.rx_buf_pos_byte--;
        }
        uart_rx_buffer[uart_data.rx_buf_pos_byte] = 0;
        uart_clear_rx();
        return uart_rx_buffer;

      }
      else
      {
        uart_clear_rx();
        return NULL;
      }
      
    }
  }
  return NULL;
  #endif
}
#endif