#ifndef _PWM_UART_H
#define _PWM_UART_H

#if 0
#define ENABLE_PRINT
#include <stdint.h>
#include "pwm_uart_cfg.h"
#include "global.h"
typedef struct
{
  uint8_t pwm_cycles;
  bool enabled;
  uint8_t pin;
}pwm_data_t;

typedef enum
{
  UART_STATUS_START_BIT,
  UART_STATUS_DATA,
  UART_STATUS_STOP_BIT,
}uart_status_t;


typedef struct 
{
  bool sending;
  char * buf_p;
  uint8_t tx_buf_pos_byte;
  uint8_t tx_buf_len;
  uint8_t rx_buf_pos_byte;
}uart_data_t;

#define PRINT_BUFFER_SIZE 128
#define RX_BUFFER_SIZE 128



#if 0
#define DEBUG_PRINT_LOW_POWER
#endif

#ifdef ENABLE_PRINT
#if 1
#define PRINTF(...) {ATOMIC_BLOCK(ATOMIC_RESTORESTATE){snprintf(print_buffer + uart_data.tx_buf_len, PRINT_BUFFER_SIZE, __VA_ARGS__); uart_send(print_buffer);}}
#define PRINT_I32(text, num){print_num_i32(text, num);}
#define PRINT(text){ATOMIC_BLOCK(ATOMIC_RESTORESTATE){strncpy(print_buffer + uart_data.tx_buf_len, text, PRINT_BUFFER_SIZE - uart_data.tx_buf_len); uart_send(print_buffer);}}
#else
#define PRINTF(...) {}
#endif
#else
#define PRINTF(...) {}
#endif

#ifdef ENABLE_PRINT
extern char print_buffer[PRINT_BUFFER_SIZE];
extern char uart_rx_buffer[RX_BUFFER_SIZE];
#endif

extern volatile pwm_data_t pwm_data;

extern volatile uart_data_t uart_data;

extern volatile uint32_t system_tick_counter;

extern void advance_system_tick_counter(uint32_t ticks);

extern uint32_t get_system_tick_counter();

extern void uart_flush();

extern void pwm_uart_init(uint8_t pwm_pin);

extern void pwm_start();

extern void pwm_stop();

extern void pwm_set_duty_pct(uint8_t pct);

extern bool uart_send(char * buf_p);

extern char* uart_readline();


extern void print_num_i32(const char *text, int32_t num);

#endif
#endif