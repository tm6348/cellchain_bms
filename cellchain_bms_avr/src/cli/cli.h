#ifndef _CLI_H
#define _CLI_H
#include "global.h"
#include "global_cfg.h"
#ifdef ENABLE_CLI

extern void cli_init();

extern char* cli_get_word(char* line, uint8_t n);
extern void cli_wordify(char* line);
extern void cli_process();

#endif
#endif