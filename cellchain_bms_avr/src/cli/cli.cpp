#include "cli/cli.h"
#include "cell_manager/all_cell_data.h"
#include"cellchain/cellchain_commands.h"
#ifdef ENABLE_CLI

#define CLI_WRITE(text){ cli_write((char*) text); }
void cli_init()
{

}
char int_convert[24];

void cli_write(char* text)
{
  uart_insert_string(text);
}
void cli_newline()
{
  uart_insert_string((char *)"\n");
}
void cli_send()
{
  uart_send();
}

bool cli_str_to_int(char* word, int32_t* num)
{
  char *ptr = word;
  while(0 != *ptr)
  {
    if(*ptr < '0' || *ptr > '9')
    {
      /* This is not a number! */
      return false;
    }
    ptr++;
  }
  /* This is a number */
  sscanf(word, "%ld", num);
  return true;
}

void cli_int_to_str(char *str, int32_t num)
{
  sprintf(str, "%ld", num);
}

void cli_write_int(int32_t num)
{
  sprintf(int_convert, "%ld", num);
  cli_write(int_convert);
}

bool cli_require_word_count(char* line, uint8_t n)
{
  /* TODO: */
  return true;
}


char* cli_get_word(char* line, uint8_t n)
{
  if(0 == n)
  {
    /* get first word */
    return line; 
  }
  else
  {
    uint8_t counter = 0;
    while(1)
    {
      if(0 == *line)
      {
        counter++;
      }
      if(counter >= n)
      {
        if(255 == (uint8_t) *(line + 1))
        {
          return NULL;
        }
        return line + 1;
      }
      if(255 == (uint8_t) *line)
      {
        return NULL;
      }
      line++;
    }
  }
}

void cli_wordify(char* line)
{
  /* We iterate over line string and replace spaces with 0 (NULL)*/
  #ifdef DEBUG_CAT_CLI_PARSE
  DEBUG_PRINTLN_INT("Wordify start", *line)
  #endif
  while(0 != *line)
  {
    if(' ' == *line) 
    {
      *line = 0;
    }
    if('\n' == *line || '\r' == *line)
    {
      /* wordified string delimitier */
      *line = 0;
      line++;
      *line = 255;
      return;
    }
    line++;
  }
}

void cli_respond_fw_ver(char *line)
{
  CLI_WRITE(FW_VER);
}


void cli_respond_numtest(char *line)
{
  CLI_WRITE("NUM test");
  cli_newline();
  int32_t num;
  if(!cli_str_to_int(cli_get_word(line, 1), &num))
  {
    CLI_WRITE("Invalid number");
    return;
  }
  cli_write_int(num + 1);
  
}
void cli_respond_cells(char *line)
{
  CLI_WRITE("NUM cells: ");
  cli_write_int(all_cell_data.num_cells);
  cli_newline();
  for(uint8_t i = 0; i < all_cell_data.num_cells; i++)
  {
    CLI_WRITE("C");
    cli_write_int(i);
    CLI_WRITE(": U:");
    cli_write_int(all_cell_data.cells[i].voltage_mv);
    CLI_WRITE(" T:");
    cli_write_int(all_cell_data.cells[i].temperature_c);
    CLI_WRITE(" UPD:")
    cli_write_int(system_timer_get_ms() - all_cell_data.cells[i].last_update);
    cli_newline();
  }
}

void cli_respond_calibrate(char *line)
{
  uint8_t target_id = 0;
  uint32_t actual_mv = 0;
  if(!cli_str_to_int(cli_get_word(line, 1), (int32_t*)&target_id))
  {
    CLI_WRITE("Invalid id")
    return;
  }
  if(!cli_str_to_int(cli_get_word(line, 2), (int32_t*)&actual_mv))
  {
    CLI_WRITE("Invalid milivolts")
    return;
  }
  CLI_WRITE("Calibrating")
  cli_newline();

  if(0 == target_id)
  {
    /* Calibrate self */
    CLI_WRITE("Calibrating self")
    adc_calibrate(actual_mv);
  }
  else
  {
    uint32_t start = system_timer_get_ms();

    cellchain_cmd_request_calibrate(target_id, actual_mv);
    CLI_WRITE("Sent calibration request")
  }

  
}

void cli_respond_enumerate(char* line)
{
  CLI_WRITE("Begin enumeration");
  cli_newline();

  cellchain_enumerate_bus();
}

void cli_process()
{
  char *line = uart_readline();
  if(NULL == line)
  {
    return;
  }
  cli_wordify(line);
  char *cmd = cli_get_word(line, 0);
  #ifdef DEBUG_CAT_CLI_PARSE
  DEBUG_PRINT("cli cmd: ")
  DEBUG_PRINT(cmd);
  DEBUG_PRINT("end")
  #endif
  if(0 == strcmp("ver", cmd) && cli_require_word_count(line, 1))
  {
    #ifdef DEBUG_CAT_CLI_PARSE
    DEBUG_PRINTLN("VER CMD")
    #endif
    cli_respond_fw_ver(line);
  }
  else if(0 == strcmp("numtest", cmd) && cli_require_word_count(line, 2))
  {
    cli_respond_numtest(line);
  }
  else if(0 == strcmp("cells", cmd) && cli_require_word_count(line, 1))
  {
    cli_respond_cells(line);
  }
  else if(0 == strcmp("enum", cmd) && cli_require_word_count(line, 1))
  {
    cli_respond_enumerate(line);
  }
  else if(0 == strcmp("calib", cmd) && cli_require_word_count(line, 3))
  {
    cli_respond_calibrate(line);
  }
  else
  {
    CLI_WRITE("Invalid command");
  }
  cli_newline();
  cli_send();
}

#endif