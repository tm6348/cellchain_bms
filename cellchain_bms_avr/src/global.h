#ifndef _GLOBAL_H
#define _GLOBAL_H

#include "avr/io.h"
#include "avr/interrupt.h"
#include "avr/sleep.h"
#include "avr/wdt.h"
#include "util/delay.h"
#include "util/atomic.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"
#include "cellchain/cellchain.h"
#include "cli/cli.h"
#include "cell_manager/cell_manager.h"
#include "power_manager/power_manager.h"
#include "adc/adc.h"
#include "pwm/pwm.h"
#include "main.h"
#include "pins.h"
#include "zoompin.h"
#include "debug/debug.h"
#include "uart/uart.h"



#endif