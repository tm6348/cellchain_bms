#include "zoompin.h"


#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
uint8_t zoompin_pinchange_int0_count = 0;
uint8_t zoompin_pinchange_int1_count = 0;
uint8_t zoompin_pinchange_int2_count = 0;
#endif
void write_reg(volatile uint8_t *reg, uint8_t offset, uint8_t value)
{
  if(value)
  {
    *reg |= 1 << offset;
  }
  else
  {
    *reg &= ~(1 << offset);
  }
}
uint8_t read_reg(volatile uint8_t *reg, uint8_t offset)
{
  return (*reg) >> offset & 1;
}
void toggle_reg(volatile uint8_t *reg, uint8_t offset)
{
  *reg ^= 1 << offset;
}