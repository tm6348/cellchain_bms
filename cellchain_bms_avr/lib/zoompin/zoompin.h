#ifndef _ZOOMPIN_H
#define _ZOOMPIN_H

#include "stdint.h"

#include "avr/io.h"

typedef enum
{
  PIN_MODE_INPUT = 0,
  PIN_MODE_OUTPUT,
}pin_mode_t;

typedef enum
{
  INT_STATE_DISABLED = 0,
  INT_STATE_ENABLED,
}int_state_t;

extern void write_reg(volatile uint8_t *reg, uint8_t offset, uint8_t value);

extern uint8_t read_reg(volatile uint8_t *reg, uint8_t offset);

extern void toggle_reg(volatile uint8_t *reg, uint8_t offset);


#define WRITE_PIN(pin, value) XWRITE_PIN(pin,value)
#define XWRITE_PIN(pin, value) YWRITE_PIN_##pin(value)

#define READ_PIN(pin) XREAD_PIN(pin)
#define XREAD_PIN(pin) YREAD_PIN_##pin

#define SET_PINMODE(pin, mode) XSET_PINMODE(pin, mode)
#define XSET_PINMODE(pin, mode) YSET_PINMODE_##pin(mode)

#define PINCHANGE_INT_SET(pin, int_state) XPINCHANGE_INT_SET(pin, int_state)
#define XPINCHANGE_INT_SET(pin, int_state) YPINCHANGE_INT_SET_##pin(int_state)

#define PINCHANGE_INT_GET_GROUP(pin) XPINCHANGE_INT_GET_GROUP(pin)
#define XPINCHANGE_INT_GET_GROUP(pin) YPINCHANGE_INT_GET_GROUP_##pin

#define PINCHANGE_INT_GET_MSK_REG(pin) XPINCHANGE_INT_GET_MSK_REG(pin)
#define XPINCHANGE_INT_GET_MSK_REG(pin) YPINCHANGE_INT_GET_MSK_REG_##pin

#define PINCHANGE_INT_GET_GROUP_ISR(pin) XPINCHANGE_INT_GET_GROUP_ISR(pin)
#define XPINCHANGE_INT_GET_GROUP_ISR(pin) YPINCHANGE_INT_GET_GROUP_ISR_##pin

#define PINCAHNGE_INT_GET_INDEX(pin) XPINCHANGE_INT_GET_INDEX(pin)
#define XPINCHANGE_INT_GET_INDEX(pin) YPINCHANGE_INT_GET_INDEX_##pin

#define TOGGLE_PIN(pin) XTOGGLE_PIN(pin)
#define XTOGGLE_PIN(pin) YTOGGLE_PIN_##pin


#if defined(__AVR_ATmega328__) || defined(__AVR_ATmega328P__)

  #define YWRITE_PIN_0(value) write_reg(&PORTD, 0, value);
  #define YWRITE_PIN_1(value) write_reg(&PORTD, 1, value);
  #define YWRITE_PIN_2(value) write_reg(&PORTD, 2, value);
  #define YWRITE_PIN_3(value) write_reg(&PORTD, 3, value);
  #define YWRITE_PIN_4(value) write_reg(&PORTD, 4, value);
  #define YWRITE_PIN_5(value) write_reg(&PORTD, 5, value);
  #define YWRITE_PIN_13(value) write_reg(&PORTB, 5, value);

  #define YTOGGLE_PIN_0 toggle_reg(&PORTD, 0);
  #define YTOGGLE_PIN_1 toggle_reg(&PORTD, 1);
  #define YTOGGLE_PIN_2 toggle_reg(&PORTD, 2);
  #define YTOGGLE_PIN_3 toggle_reg(&PORTD, 3);
  #define YTOGGLE_PIN_4 toggle_reg(&PORTD, 4);
  #define YTOGGLE_PIN_5 toggle_reg(&PORTD, 5);
  #define YTOGGLE_PIN_13 toggle_reg(&PORTB, 5);

  #define YREAD_PIN_0 read_reg(&PIND, 0);
  #define YREAD_PIN_1 read_reg(&PIND, 1);
  #define YREAD_PIN_2 read_reg(&PIND, 2);
  #define YREAD_PIN_3 read_reg(&PIND, 3);
  #define YREAD_PIN_4 read_reg(&PIND, 4);
  #define YREAD_PIN_5 read_reg(&PIND, 5);
  #define YREAD_PIN_13 read_reg(&PINB, 4);

  #define YSET_PINMODE_0(mode) write_reg(&DDRD, 0, mode);
  #define YSET_PINMODE_1(mode) write_reg(&DDRD, 1, mode);
  #define YSET_PINMODE_2(mode) write_reg(&DDRD, 2, mode);
  #define YSET_PINMODE_3(mode) write_reg(&DDRD, 3, mode);
  #define YSET_PINMODE_4(mode) write_reg(&DDRD, 4, mode);
  #define YSET_PINMODE_5(mode) write_reg(&DDRD, 5, mode);
  #define YSET_PINMODE_13(mode) write_reg(&DDRB, 5, mode);

  #define YPINCHANGE_INT_GET_GROUP_0 2
  #define YPINCHANGE_INT_GET_GROUP_1 2
  #define YPINCHANGE_INT_GET_GROUP_2 2
  #define YPINCHANGE_INT_GET_GROUP_3 2
  #define YPINCHANGE_INT_GET_GROUP_4 2
  #define YPINCHANGE_INT_GET_GROUP_5 2

  #define YPINCHANGE_INT_GET_MSK_REG_0 PCMSK2
  #define YPINCHANGE_INT_GET_MSK_REG_1 PCMSK2
  #define YPINCHANGE_INT_GET_MSK_REG_2 PCMSK2
  #define YPINCHANGE_INT_GET_MSK_REG_3 PCMSK2
  #define YPINCHANGE_INT_GET_MSK_REG_4 PCMSK2
  #define YPINCHANGE_INT_GET_MSK_REG_5 PCMSK2

  #define YPINCHANGE_INT_GET_GROUP_ISR_0 PCINT2_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_1 PCINT2_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_2 PCINT2_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_3 PCINT2_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_4 PCINT2_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_5 PCINT2_vect

  #define YPINCHANGE_INT_GET_INDEX_0 0
  #define YPINCHANGE_INT_GET_INDEX_1 1
  #define YPINCHANGE_INT_GET_INDEX_2 2
  #define YPINCHANGE_INT_GET_INDEX_3 3
  #define YPINCHANGE_INT_GET_INDEX_4 4
  #define YPINCHANGE_INT_GET_INDEX_5 5

  #if 0 /* TODO: consider if required */
  extern uint8_t zoompin_pinchange_int0_count;

  #define PINCHANGE_INT_HANDLE_PCIE_0(int_state, int_index) {\
    if(int_state)\
    {\
      zoompin_pinchange_int0_count--;\
      if(!zoompin_pinchange_int0_count)\
      {\
        PCICR &= ~(1 << PCIE0);\
      }\
      PCMSK0 &= ~(1 << int_index);\
    }\
    else\
    {\
      PCICR |= 1 << PCIE0;\
      PCMSK0 |= 1 << int_index;\
      zoompin_pinchange_int0_count++;\
    }\
  }
  #define PINCHANGE_INT_SET_0(int_state) PINCHANGE_INT_HANDLE_PCIE_0(int_state, 0)
  #define PINCHANGE_INT_SET_1(int_state) PINCHANGE_INT_HANDLE_PCIE_0(int_state, 1)
  #define PINCHANGE_INT_SET_2(int_state) PINCHANGE_INT_HANDLE_PCIE_0(int_state, 2)
  #endif




#elif defined(__AVR_ATtiny85__)

  /*TODO: */
  #define YWRITE_PIN_0(value) write_reg(&DDRB, 0, value);
  #define YWRITE_PIN_1(value) write_reg(&DDRB, 1, value);
  #define YWRITE_PIN_2(value) write_reg(&DDRB, 2, value);
  #define YWRITE_PIN_3(value) write_reg(&DDRB, 3, value);
  #define YWRITE_PIN_4(value) write_reg(&DDRB, 4, value);
  #define YWRITE_PIN_5(value) write_reg(&DDRB, 5, value);


  #define YREAD_PIN_0 read_reg(&PINB, 0);
  #define YREAD_PIN_1 read_reg(&PINB, 1);
  #define YREAD_PIN_2 read_reg(&PINB, 2);
  #define YREAD_PIN_3 read_reg(&PINB, 3);
  #define YREAD_PIN_4 read_reg(&PINB, 4);
  #define YREAD_PIN_5 read_reg(&PINB, 5);


#elif defined(__AVR_ATtiny84__)
  #define YWRITE_PIN_0(value) write_reg(&PORTA, 0, value);
  #define YWRITE_PIN_1(value) write_reg(&PORTA, 1, value);
  #define YWRITE_PIN_2(value) write_reg(&PORTA, 2, value);
  #define YWRITE_PIN_3(value) write_reg(&PORTA, 3, value);
  #define YWRITE_PIN_4(value) write_reg(&PORTA, 4, value);
  #define YWRITE_PIN_5(value) write_reg(&PORTA, 5, value);
  #define YWRITE_PIN_6(value) write_reg(&PORTA, 6, value);
  #define YWRITE_PIN_7(value) write_reg(&PORTA, 7, value);
  #define YWRITE_PIN_8(value) write_reg(&PORTB, 2, value);
  #define YWRITE_PIN_9(value) write_reg(&PORTB, 1, value);
  #define YWRITE_PIN_10(value) write_reg(&PORTB, 0, value);
  #define YWRITE_PIN_11(value) write_reg(&PORTB, 3, value);

  #define YTOGGLE_PIN_0 toggle_reg(&PORTA, 0);
  #define YTOGGLE_PIN_1 toggle_reg(&PORTA, 1);
  #define YTOGGLE_PIN_2 toggle_reg(&PORTA, 2);
  #define YTOGGLE_PIN_3 toggle_reg(&PORTA, 3);
  #define YTOGGLE_PIN_4 toggle_reg(&PORTA, 4);
  #define YTOGGLE_PIN_5 toggle_reg(&PORTA, 5);
  #define YTOGGLE_PIN_6 toggle_reg(&PORTA, 6);
  #define YTOGGLE_PIN_7 toggle_reg(&PORTA, 7);
  #define YTOGGLE_PIN_8 toggle_reg(&PORTB, 2);
  #define YTOGGLE_PIN_9 toggle_reg(&PORTB, 1);
  #define YTOGGLE_PIN_10 toggle_reg(&PORTB, 0);
  #define YTOGGLE_PIN_11 toggle_reg(&PORTB, 3);

  #define YREAD_PIN_0 read_reg(&PINA, 0);
  #define YREAD_PIN_1 read_reg(&PINA, 1);
  #define YREAD_PIN_2 read_reg(&PINA, 2);
  #define YREAD_PIN_3 read_reg(&PINA, 3);
  #define YREAD_PIN_4 read_reg(&PINA, 4);
  #define YREAD_PIN_5 read_reg(&PINA, 5);
  #define YREAD_PIN_6 read_reg(&PINA, 6);
  #define YREAD_PIN_7 read_reg(&PINA, 7);
  #define YREAD_PIN_8 read_reg(&PINB, 2);
  #define YREAD_PIN_9 read_reg(&PINB, 1);
  #define YREAD_PIN_10 read_reg(&PINB, 0);
  #define YREAD_PIN_11 read_reg(&PINB, 3);

  #define YSET_PINMODE_0(value) write_reg(&DDRA, 0, value);
  #define YSET_PINMODE_1(value) write_reg(&DDRA, 1, value);
  #define YSET_PINMODE_2(value) write_reg(&DDRA, 2, value);
  #define YSET_PINMODE_3(value) write_reg(&DDRA, 3, value);
  #define YSET_PINMODE_4(value) write_reg(&DDRA, 4, value);
  #define YSET_PINMODE_5(value) write_reg(&DDRA, 5, value);
  #define YSET_PINMODE_6(value) write_reg(&DDRA, 6, value);
  #define YSET_PINMODE_7(value) write_reg(&DDRA, 7, value);
  #define YSET_PINMODE_8(value) write_reg(&DDRB, 2, value);
  #define YSET_PINMODE_9(value) write_reg(&DDRB, 1, value);
  #define YSET_PINMODE_10(value) write_reg(&DDRB, 0, value);
  #define YSET_PINMODE_11(value) write_reg(&DDRB, 3, value);

  #define YPINCHANGE_INT_GET_GROUP_0 0
  #define YPINCHANGE_INT_GET_GROUP_1 0
  #define YPINCHANGE_INT_GET_GROUP_2 0
  #define YPINCHANGE_INT_GET_GROUP_3 0
  #define YPINCHANGE_INT_GET_GROUP_4 0
  #define YPINCHANGE_INT_GET_GROUP_5 0
  #define YPINCHANGE_INT_GET_GROUP_6 0
  #define YPINCHANGE_INT_GET_GROUP_7 0
  #define YPINCHANGE_INT_GET_GROUP_8 1
  #define YPINCHANGE_INT_GET_GROUP_9 1
  #define YPINCHANGE_INT_GET_GROUP_10 1

  #define YPINCHANGE_INT_GET_MSK_REG_0 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_1 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_2 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_3 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_4 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_5 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_6 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_7 PCMSK0
  #define YPINCHANGE_INT_GET_MSK_REG_8 PCMSK1
  #define YPINCHANGE_INT_GET_MSK_REG_9 PCMSK1
  #define YPINCHANGE_INT_GET_MSK_REG_10 PCMSK1

  #define YPINCHANGE_INT_GET_GROUP_ISR_0 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_1 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_2 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_3 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_4 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_5 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_6 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_7 PCINT0_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_8 PCINT1_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_9 PCINT1_vect
  #define YPINCHANGE_INT_GET_GROUP_ISR_10 PCINT1_vect

  #define YPINCHANGE_INT_GET_INDEX_0 0
  #define YPINCHANGE_INT_GET_INDEX_1 1
  #define YPINCHANGE_INT_GET_INDEX_2 2
  #define YPINCHANGE_INT_GET_INDEX_3 3
  #define YPINCHANGE_INT_GET_INDEX_4 4
  #define YPINCHANGE_INT_GET_INDEX_5 5
  #define YPINCHANGE_INT_GET_INDEX_6 6
  #define YPINCHANGE_INT_GET_INDEX_7 7
  #define YPINCHANGE_INT_GET_INDEX_8 2
  #define YPINCHANGE_INT_GET_INDEX_9 1
  #define YPINCHANGE_INT_GET_INDEX_10 0

#endif


#endif